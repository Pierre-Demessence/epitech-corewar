/*
** ins_noidxmod.c for Corewar/VM in /home/demess_p/Projects/C/Corewar/VM
** 
** Made by demess_p
** Login   <demess_p@epitech.net>
** 
** Started on  Mon Dec  2 19:46:05 2013 demess_p
** Last update Sun Dec 15 16:35:03 2013 demess_p
*/

#include "mem_utils.h"
#include "my_vm.h"
#include "parm_utils.h"

unsigned char	lld(t_champion *c)
{
  const t_parms	*parms;

  parms = get_parms(c, PARM2_IDX);
  if (!parms->error)
    {
      c->carry = (parms->p[0] == 0 ? 1 : 0);
      c->registers[parms->p[1]] = parms->p[0];
    }
  return (1 + 1 + parms->size);
}

unsigned char	lldi(t_champion *c)
{
  const t_parms	*parms;
  int		value;

  parms = get_parms(c, PARM3_IDX | PARM_IDXS | PARM_LONG);
  if (!parms->error)
    {
      value = mem_readi(REG_SIZE, parms->p[0] + parms->p[1]);
      c->carry = (value == 0 ? 1 : 0);
      c->registers[parms->p[2]] = value;
    }
  return (1 + 1 + parms->size);
}

unsigned char	lfork(t_champion *c_cur)
{
  unsigned int	idx;
  t_champion	*c_new;

  idx = mem_readi(2, c_cur->pc + 1);
  c_new = copy_champion(c_cur);
  c_new->pc = (c_cur->pc + idx) % MEM_SIZE;
  c_new->carry = 0;
  c_cur->carry = 1;
  return (1 + 2);
}
