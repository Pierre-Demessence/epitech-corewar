/*
** options_functions.c for corewar in /home/taieb_t/Desktop/corewar/VM
** 
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
** 
** Started on  Sun Dec  8 15:16:50 2013 taieb_t$
** Last update Sun Dec 15 20:19:27 2013 taieb_t$
*/

#include <unistd.h>
#include <stdlib.h>
#include "my_vm.h"

static char	*g_options_number[] =
  {
    "-dump",
    "-pourcent",
    "-ctmo",
    "-noX",
  };

static char	*g_options_basic[] =
  {
    "-comment",
    "-by_step",
    "-aff",
    "-aff_shell",
    "-aff_proc",
    "-graphics"
  };

int		get_champi_number(t_char_list *paths)
{
  int		i;

  i = 0;
  while (paths != NULL)
    {
      if (my_strcmp(paths->str, "champi") == 0)
	i = i + 1;
      paths = paths->next;
    }
  return (i);
}

char		check_n_or_a_options(t_options *options, char *str,
				     char *strp, int champi)
{
  if (my_strcmp("-n", str) == 0)
    {
      if (strp != NULL)
	options->numbers[champi] = my_getnbr(strp);
      return (1);
    }
  else if (my_strcmp("-a", str) == 0)
    {
      if (strp != NULL)
	options->loader[champi] = my_getnbr(strp);
      return (1);
    }
  return (0);
}

char		add_an_option(t_options *options, char *str,
			      char *strp, int champi)
{
  int		opts;

  opts = -1;
  while (++opts < 6)
    if (my_strcmp(str, g_options_basic[opts]) == 0)
      {
	options->options_basic |= 1 << opts;
	return (0);
      }
  opts = -1;
  while (++opts < 4)
    if (my_strcmp(str, g_options_number[opts]) == 0)
      {
	options->options_advanced |= 1 << opts;
	if (strp != NULL)
	  options->values[opts] = my_getnbr(strp);
	return (1);
      }
  return (check_n_or_a_options(options, str, strp, champi));
}

t_options	*init_my_options(t_char_list *paths)
{
  int		nb;
  t_options	*options;

  nb = get_champi_number(paths);
  if ((options = my_malloc(sizeof(t_options))) == NULL)
    return (NULL);
  options->options_basic = 0;
  options->options_advanced = 0;
  if ((options->numbers = my_malloc(sizeof(int) * (nb + 1))) == NULL)
    return (NULL);
  if ((options->loader = my_malloc(sizeof(int) * (nb + 1))) == NULL)
    return (NULL);
  while (nb >= 0)
    {
      options->numbers[nb] = -1;
      options->loader[nb] = -1;
      nb = nb - 1;
    }
  return (options);
}

t_options	*set_my_options(t_char_list *paths)
{
  t_options	*options;
  int		count_champi;

  if ((options = init_my_options(paths)) == NULL)
    return (NULL);
  count_champi = 0;
  while (paths != NULL)
    {
      if (my_strcmp(paths->str, "champi") == 0)
	count_champi = count_champi + 1;
      if (paths->next == NULL)
	add_an_option(options, paths->str, NULL, count_champi);
      else
	{
	  if (add_an_option(options, paths->str, paths->next->str, count_champi))
	    paths = paths->next;
	}
      paths = paths->next;
    }
  return (options);
}
