/*
** sdl_ttf.c for Corewar/VM in /home/demess_p/Projects/C/Corewar/VM
** 
** Made by demess_p
** Login   <demess_p@epitech.net>
** 
** Started on  Fri Dec 13 16:12:16 2013 demess_p
** Last update Sun Dec 15 18:42:51 2013 demess_p
*/

#include <SDL/SDL_ttf.h>
#include "my_vm.h"
#include "sdl.h"
#include "string_utils.h"

int	ttf_init()
{
  if (TTF_Init())
    return (0);
  if (!(g_interface.font = TTF_OpenFont("dejavusans.ttf", TTF_SIZE)))
    return (0);
  return (1);
}

static SDL_Color	ttf_get_color(int colori)
{
  static SDL_Color	color;

  color.r = (colori & 0xFF0000) >> 16;
  color.g = (colori & 0x00FF00) >> 8;
  color.b = (colori & 0x0000FF);
  return (color);
}

void		ttf_draw_text(char *str, int x, int y, int color)
{
  SDL_Surface	*text;
  SDL_Rect	pos;

  if (!(text = TTF_RenderText_Solid(g_interface.font, str,
				    ttf_get_color(color))))
    return ;
  pos.x = x;
  pos.y = y;
  (void)SDL_BlitSurface(text, NULL, g_interface.screen, &pos);
  SDL_FreeSurface(text);
}

static void	ttf_draw_bis(t_scheduling *data)
{
  t_list	*tmp;
  int		i;

  ttf_draw_text(my_strconcat("Cycle : ",
			     my_nbrtostr(data->total_cycles, "0123456789")),
		(WIN_WIDTH - WIN_BRD), TTF_SIZE * 0, 0xFFFFFF);
  ttf_draw_text(my_strconcat("Lives : ",
			     my_nbrtostr(data->total_lives, "0123456789")),
		(WIN_WIDTH - WIN_BRD), TTF_SIZE * 1, 0xFFFFFF);
  ttf_draw_text(my_strconcat("C.T.D : ",
			     my_nbrtostr(data->cycles_to_die, "0123456789")),
		(WIN_WIDTH - WIN_BRD), TTF_SIZE * 2, 0xFFFFFF);
  tmp = g_champions;
  while (tmp && tmp->champion)
    {
      if (tmp->champion->life >= 0)
	++i;
      tmp = tmp->next;
    }
  ttf_draw_text(my_strconcat("N Prc : ",
			     my_nbrtostr(i, "0123456789")),
		(WIN_WIDTH - WIN_BRD), TTF_SIZE * 3, 0xFFFFFF);
}

void		ttf_draw(t_scheduling *data)
{
  t_list	*tmp;
  int		i;
  int		ypos;

  ttf_draw_bis(data);
  i = 0;
  tmp = g_champions;
  ypos = 0;
  while (tmp && tmp->champion && ypos < (WIN_HEIGHT - TTF_SIZE))
    {
      if (tmp->champion->life != -1)
	{
	  ypos = 75 + (TTF_SIZE * 1) * i;
	  ttf_draw_text(tmp->champion->name,
			(WIN_WIDTH - WIN_BRD), ypos,
			sdl_get_color(tmp->champion->num_player, COLOR_COMP));
	  ++i;
	}
      tmp = tmp->next;
    }
}
