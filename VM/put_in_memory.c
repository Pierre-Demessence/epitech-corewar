/*
** put_in_memory.c for corewar in /home/taieb_t/rendu/Corewar
** 
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
** 
** Started on  Tue Dec  3 15:28:44 2013 taieb_t$
** Last update Sun Dec 15 21:46:22 2013 taieb_t$
*/

#include <unistd.h>
#include <stdlib.h>
#include "my_vm.h"
#include "sdl.h"

int		get_size(t_buffer **buffers, int max)
{
  int		res;
  int		i;

  res = MEM_SIZE;
  i = 0;
  while (i < max)
    {
      if (buffers[i] != NULL)
	res = res - buffers[i]->size;
      i = i + 1;
    }
  if (res <= 0)
    return (0);
  res = res / i;
  return (res);
}

void		buff_in_memo(t_buffer *buffer, unsigned int *pos,
			     int num_player)
{
  int		i;

  i = 0;
  while (i < buffer->size)
    {
      g_memory[(*pos % MEM_SIZE)] = buffer->buffer[i];
#ifdef BONUS
      g_interface.cases[(*pos % MEM_SIZE)] = num_player;
#endif
      *pos = *pos + 1;
      i = i + 1;
    }
  (void)num_player;
}

void		init_memory()
{
  int		i;

  i = 0;
  while (i < MEM_SIZE)
    {
      g_memory[i] = 0;
      i = i + 1;
    }
}

char		my_put_in_memory(t_buffer **buffers, t_list *champs,
				 int sizemax, t_options *opts)
{
  int		free_size;
  unsigned int	pos;
  int		i;

  init_memory();
  i = 0;
  if ((free_size = get_size(buffers, sizemax)) == 0)
    {
      my_put_errstr("Memory to small to contain all programs\n");
      return (1);
    }
  pos = 0;
  i = sizemax;
  while (--i >= 0)
    {
      if (opts->loader[i] != -1)
	pos = (opts->loader[i] % MEM_SIZE);
      champs->champion->pc = pos;
      buff_in_memo(buffers[i], &pos, champs->champion->num_player);
      if (champs->next != NULL)
	champs = champs->next;
      pos = (pos + free_size) % MEM_SIZE;
    }
  return (0);
}
