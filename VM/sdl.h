/*
** sdl.h for Corewar/VM in /home/demess_p/Projects/Corewar/VM
** 
** Made by demess_p
** Login   <demess_p@epitech.net>
** 
** Started on  Sat Dec  7 16:08:03 2013 demess_p
** Last update Sun Dec 15 21:01:59 2013 demess_p
*/

#ifndef SDL_H_
# define SDL_H_

# include <SDL/SDL.h>
# include <SDL/SDL_ttf.h>
# include "op.h"

# define WIN_WIDTH	1000
# define WIN_HEIGHT	1000

# define WIN_BRD	150

# define CASE_WIDTH	10
# define CASE_HEIGHT	10

# define PC_WIDTH	6
# define PC_HEIGHT	6

# define NB_COLORS	10

# define COLOR_CASE	0
# define COLOR_COMP	1

typedef struct		s_sdl
{
  SDL_Surface		*screen;
  char			cases[MEM_SIZE];
  SDL_Surface		*rcase;
  SDL_Surface		*rpc;
  TTF_Font		*font;
}			t_sdl;

extern t_sdl	g_interface;

int	sdl_init();
void	sdl_draw(t_scheduling *data);
int	sdl_free();
int	sdl_get_color(int num_player, char ispc);

# define TTF_SIZE	15

int	ttf_init();
void	ttf_draw(t_scheduling *data);
void    ttf_draw_text(char *str, int x, int y, int color);

#endif /* !SDL_H_ */
