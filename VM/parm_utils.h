/*
** parm_utils.h for Corewar/VM in /home/demess_p/Projects/C/Corewar/VM
** 
** Made by demess_p
** Login   <demess_p@epitech.net>
** 
** Started on  Thu Dec  5 16:46:14 2013 demess_p
** Last update Sun Dec 15 23:10:36 2013 demess_p
*/

#ifndef PARM_UTILS_H_
# define PARM_UTILS_H_

# include "my_vm.h"
# include "op.h"

# define PARM1_IDX	0x01
# define PARM2_IDX	0x02
# define PARM3_IDX	0x04
# define PARM4_IDX	0x08
# define PARM_LONG	0x10
# define PARM_IDXS	0x20

# define FLAG_IDX	0x01
# define FLAG_LNG	0x02

typedef struct	s_parms
{
  int		p[MAX_ARGS_NUMBER];
  char		t[MAX_ARGS_NUMBER];
  char		s[MAX_ARGS_NUMBER];
  char		size;
  char		offset;
  char		error;
}		t_parms;

const t_parms	*get_parms(t_champion *c, char flags);

#endif /* !PARM_UTILS_H_ */
