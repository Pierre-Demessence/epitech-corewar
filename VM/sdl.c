/*
** main.c for Corewar/VM in /home/demess_p/Projects/C/Corewar/VM
** 
** Made by demess_p
** Login   <demess_p@epitech.net>
** 
** Started on  Sat Dec  7 14:18:20 2013 demess_p
** Last update Sun Dec 15 21:20:23 2013 demess_p
*/

#include <signal.h>
#include <SDL/SDL.h>
#include "my_vm.h"
#include "sdl.h"

t_sdl	g_interface;

static const int	colors[NB_COLORS] =
  {
    0xFF0000,
    0x00FF00,
    0x3333FF,
    0xFFFF00,
    0x00FFFF,
    0xFF00FF,
    0xE67E30,
    0x88421D,
    0xFF5E5D,
    0x8D4024
  };

int	sdl_get_color(int num_player, char ispc)
{
  if (ispc == COLOR_CASE)
    return ((colors[(num_player - 1) % NB_COLORS] & 0xFEFEFE) >> 1);
  return (colors[(num_player - 1) % NB_COLORS]);
}

static void		sdl_draw_case(int i, t_champion *c, SDL_Surface *r)
{
  static SDL_Rect	pos;
  int			color;

  if (!c)
    color = (g_interface.cases[i] == 0 ?
	     0x555555 :
	     sdl_get_color(g_interface.cases[i], COLOR_CASE));
  else if (c->life != -1)
    color = sdl_get_color(c->num_player, COLOR_COMP);
  else
    color = 0x000000;
  pos.x = (i % ((WIN_WIDTH - WIN_BRD) / (CASE_WIDTH + 1))) * (CASE_WIDTH + 1)
    + ((CASE_WIDTH - r->w) / 2);
  pos.y = (i / ((WIN_WIDTH - WIN_BRD) / (CASE_WIDTH + 1))) * (CASE_HEIGHT + 1)
    + ((CASE_HEIGHT - r->h) / 2);
  (void)SDL_FillRect(r, NULL, color);
  (void)SDL_BlitSurface(r, NULL, g_interface.screen, &pos);
}

void		sdl_draw(t_scheduling *data)
{
  t_list	*tmp;
  int		i;

  (void)SDL_FillRect(g_interface.screen, NULL, 0x000000);
  i = 0;
  while (i < MEM_SIZE)
    sdl_draw_case(i++, NULL, g_interface.rcase);
  tmp = g_champions;
  while (tmp && tmp->champion)
    {
      sdl_draw_case(tmp->champion->pc, tmp->champion, g_interface.rpc);
      tmp = tmp->next;
    }
  ttf_draw(data);
  (void)SDL_Flip(g_interface.screen);
}

int   	sdl_init()
{
  if (SDL_Init(SDL_INIT_VIDEO))
    return (0);
  if (signal(SIGINT, SIG_DFL) == SIG_ERR)
    my_put_errstr("Warning : SIGINT Signal catched by SDL.\n");
  g_interface.screen = SDL_SetVideoMode(WIN_WIDTH, WIN_HEIGHT,
					32, SDL_HWSURFACE);
  if (!g_interface.screen)
    return (0);
  if (!ttf_init() && sdl_free())
    return (0);
  g_interface.rcase = SDL_CreateRGBSurface(SDL_HWSURFACE,
					   CASE_WIDTH, CASE_HEIGHT,
					   32, 0, 0, 0, 0);
  if (!g_interface.rcase && sdl_free())
    return (0);
  g_interface.rpc = SDL_CreateRGBSurface(SDL_HWSURFACE,
					 PC_WIDTH, PC_HEIGHT,
					 32, 0, 0, 0, 0);
  if (!g_interface.rpc && sdl_free())
    return (0);
  SDL_WM_SetCaption("Uber CoreWar", NULL);
  return (1);
}

int	sdl_free()
{
  TTF_CloseFont(g_interface.font);
  TTF_Quit();
  if (g_interface.rcase)
    SDL_FreeSurface(g_interface.rcase);
  if (g_interface.rpc)
    SDL_FreeSurface(g_interface.rpc);
  SDL_Quit();
  return (1);
}
