/*
** my_getnbr.c for Day 04 in /home/thing-_a/rendu/Piscine-C-Jour_04
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Thu Oct  3 09:45:05 2013 a
** Last update Wed Dec 11 19:31:38 2013 taieb_t$
*/

#include "my_vm.h"

int	get_true_number(char *str)
{
  int	i;
  int	len;

  len = my_strlen(str);
  i = 0;
  while (i < len)
    {
      if (str[i] < '0' || str[i] > '9')
	return (i);
      i = i + 1;
    }
  return (i);
}

int	get_limit(char *str, int len, int sign)
{
  char	*max_int;
  int	i;

  max_int = "2147483648";
  if (len > 10)
    return (0);
  if (len < 10)
    return (len);
  i = 0;
  while (max_int[i] != '\0')
    {
      if (str[i] > max_int[i])
	return (0);
      i = i + 1;
    }
  if (str[i - 1] == max_int[i - 1] && sign >= 0)
    return (0);
  return (len);
}

int	get_sign(char **str)
{
  int	nb_minus;
  int	sign;

  nb_minus = 0;
  sign = 1;
  while (**str != '\0' && (**str == '+' || **str == '-'))
    {
      if (**str == '-')
	nb_minus = nb_minus + 1;
      *str = *str + 1;
    }
  if (nb_minus % 2 != 0)
    sign = -1;
  return (sign);
}

int	my_getnbr(char *str)
{
  int	result;
  int	len;
  int	i;
  int	sign;

  sign = 1;
  i = 0;
  sign = get_sign(&str);
  len = get_true_number(str);
  len = get_limit(str, len, sign);
  result = 0;
  if (len <= 0)
    return (0);
  while (i < len)
    {
      result = (result * 10) + (str[i] - '0');
      i = i + 1;
    }
  return (result * sign);
}
