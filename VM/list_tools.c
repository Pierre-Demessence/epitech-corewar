/*
** list_tools.c for corewar in /home/taieb_t/rendu/Corewar
** 
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
** 
** Started on  Tue Dec  3 15:18:26 2013 taieb_t$
** Last update Sun Dec 15 20:18:11 2013 taieb_t$
*/

#include <unistd.h>
#include <stdlib.h>
#include "my_vm.h"

int		my_put_in_list(t_list **list, t_champion *param)
{
  t_list	*elem;

  elem = my_malloc(sizeof(t_list));
  if (elem == NULL)
    return (1);
  elem->champion = param;
  elem->next = (*list);
  (*list) = elem;
  return (0);
}

int		my_add_to_list(t_list **list, t_champion *param)
{
  t_list	*elem;
  t_list	*memo;

  memo = (*list);
  elem = my_malloc(sizeof(t_list));
  if (elem == NULL)
    return (1);
  elem->champion = param;
  elem->next = NULL;
  if ((*list) == NULL)
    (*list) = elem;
  else
    {
      while ((*list)->next != NULL)
	(*list) = (*list)->next;
      (*list)->next = elem;
      (*list) = memo;
    }
  return (0);
}

int		nb_elem_in_list(t_list *champions)
{
  int		i;

  i = 0;
  while (champions != NULL)
    {
      i = i + 1;
      champions = champions->next;
    }
  return (i);
}

int		nb_elem_in_path_list(t_char_list *paths)
{
  int		i;

  i = 0;
  while (paths != NULL)
    {
      i = i + 1;
      paths = paths->next;
    }
  return (i);
}
