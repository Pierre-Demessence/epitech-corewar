/*
** my_vm.c for Corewar in /home/taieb_t/rendu/Corewar
** 
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
** 
** Started on  Sat Nov 16 16:00:05 2013 taieb_t$
** Last update Sun Dec 15 21:39:31 2013 taieb_t$
*/

#include <unistd.h>
#include <stdlib.h>
#include "my_vm.h"
#include "sdl.h"

t_list		*g_champions;
char		g_memory[MEM_SIZE];

void		set_registers(t_list *champions)
{
  int		i;

  while (champions != NULL)
    {
      if (REG_NUMBER > 1)
	champions->champion->registers[0] = champions->champion->num_player;
      i = 1;
      while (i < REG_NUMBER)
	{
	  champions->champion->registers[i] = 0;
	  i = i + 1;
	}
      champions = champions->next;
    }
}

char		init_my_datas(t_options **options, t_list **champions,
			      t_char_list *my_champi, t_char_list *my_opts)
{
  t_buffer	**buffers;

  *champions = NULL;
  if ((buffers = init_all_champ_buffer(my_champi, champions)) == NULL)
    return (1);
  if ((*options = set_my_options(my_opts)) == NULL)
    return (1);
  if (set_number_player(*champions, *options))
    return (1);
  if (my_put_in_memory(buffers, *champions,
		       nb_elem_in_list(*champions),
		       *options))
    return (1);
  set_registers(*champions);
  my_free_buffers(buffers, nb_elem_in_list(*champions) + 1);
#ifndef BONUS
  if (((*options)->options_basic & 0x20) == 0x20)
    my_put_errstr("Please compile with \"make bonus\" if you want graphics\n");
#endif
  return (0);
}

int		main(int ac, char **av)
{
  t_char_list	*my_champi;
  t_char_list	*my_opts;
  t_options	*options;
  char		graphics;

  if (ac < 2 && show_usage())
    return (1);
  if ((my_champi = get_my_champi(ac, av)) == NULL ||
      (my_opts = get_my_options(ac, av)) == NULL)
    return (1);
  if (init_my_datas(&options, &g_champions, my_champi, my_opts))
    return (1);
  graphics = 0;
#ifdef BONUS
  if ((options->options_basic & 0x20) == 0x20)
    {
      graphics = sdl_init();
      if (!graphics)
	my_put_errstr("Sdl initialisation failed ...\n");
    }
#endif
  scheduler(options, graphics);
  my_free_all(g_champions, options, my_opts, my_champi);
  return (0);
}
