/*
** my_vm.h for corewar in /home/taieb_t/rendu/Corewar
** 
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
** 
** Started on  Tue Dec  3 15:15:07 2013 taieb_t$
** Last update Sun Dec 15 20:23:22 2013 taieb_t$
*/

#ifndef MY_VM_H_
# define MY_VM_H_

# include "op.h"

# define MAX_CYCLES 1
# define MAX_NUM_PLAYER 10

struct				s_champion
{
  char				*name;
  char				*comment;
  int				num_player;
  char				carry;
  unsigned int			pc;
  int				nb_cycle_to_wait;
  op_t				action;
  int				life;
  char				alive;
  char				registers[REG_NUMBER];
};
typedef struct s_champion	t_champion;

struct				s_list
{
  t_champion			*champion;
  struct s_list			*next;
};
typedef struct s_list		t_list;

struct				s_buffer
{
  char				*buffer;
  int				size;
};
typedef struct s_buffer		t_buffer;

struct				s_options
{
  char				options_basic;
  char				options_advanced;
  int				values[4];
  int				*numbers;
  int				*loader;
};
typedef struct s_options	t_options;

struct				s_char_list
{
  char				*str;
  struct s_char_list		*next;
};
typedef struct s_char_list	t_char_list;

struct				s_scheduling
{
  int				total_cycles;
  int				total_lives;
  int				cycles_to_die;
  int				aff_graphics;
  int				read_numbers;
};
typedef struct s_scheduling	t_scheduling;

extern t_list			*g_champions;
extern char			g_memory[MEM_SIZE];
extern t_champion		*g_winner;
extern t_scheduling		g_scheduler;

/*
** functions_sys.c
*/
void				*my_malloc(unsigned int);

/*
** free_functions.c
*/
void				my_free_list(t_list *);
void				my_free_buffers(t_buffer **, int);
void				my_free_all(t_list *, t_options *,
					    t_char_list *, t_char_list *);
void				free_my_champi(t_list *);

/*
** alives_champions.c
*/
int				get_nb_alive();
void				set_flag_alive(int);
void				show_alive_champi(t_champion *);

/*
** str_tools.c
*/
int				my_strlen(char *);
char				*my_strdup(char *);
char				*my_dupstrcat(char *, char *, int, int);
int				my_strcmp(char *, char *);
int				my_is_num_str(char *);

/*
** list_tools.c
*/
int				my_put_in_list(t_list **, t_champion *);
int				my_add_to_list(t_list **, t_champion *);
int				nb_elem_in_list(t_list *);
int				nb_elem_in_path_list(t_char_list *);

/*
** get_my_param.c
*/
t_char_list			*get_my_champi(int, char **);
t_char_list			*get_my_options(int, char **);

/*
** display_tools.c
*/
char				show_usage();
void				my_putstr(char *);
void				my_put_errstr(char *);
void				my_put_nbr(int, int);
void				my_put_error_files(char *);

/*
** my_getnbr.c
*/
int				my_getnbr(char *);

/*
** read_files.c
*/
t_buffer			**init_all_champ_buffer(t_char_list *,
							t_list **);

/*
** put_in_memory.c
*/
char				my_put_in_memory(t_buffer **, t_list *,
						 int, t_options *);

/*
** scheduling.c
*/
void				scheduler(t_options *, int);

/*
** scheduling_tools.c
*/
int				get_action_to_do(t_champion *);
void				my_show_winner(t_champion *, t_options *);
int				exist_in_op_tab(int);
void				by_step_function(t_options *, t_scheduling *);

/*
** options_schedule.c
*/
void				my_show_action(t_champion *, t_options *);
void				my_show_pc(t_champion *, t_options *);
void				my_show_action_loading(t_champion *,
						       t_options *);
void				free_my_sdl();

/*
** show_memory.c
*/
void				show_memory();

/*
** convert_to_big_endian.c
*/
short				convert_my_short_big_endian(short);
int				convert_my_int_big_endian(int);
void				my_revnchar(void *, int);

/*
** champions_functions_tools.c
*/
t_champion			*copy_champion(t_champion *);
int				my_check_corpses(t_list *);

/*
** set_numbers_players.c
*/
char				set_number_player(t_list *, t_options *);

/*
** options_functions.c
*/
t_options			*set_my_options(t_char_list *);

/*
** ins_base.c
*/
unsigned char			live(t_champion *);
unsigned char			zjump(t_champion *);
unsigned char			aff(t_champion *);
unsigned char			vm_fork(t_champion *);

/*
** ins_long.c
*/
unsigned char			lld(t_champion *);
unsigned char			lldi(t_champion *);
unsigned char			lfork(t_champion *);

/*
** ins_operations.c
*/
unsigned char			vm_add(t_champion *);
unsigned char			vm_sub(t_champion *);
unsigned char			vm_and(t_champion *);
unsigned char			vm_or(t_champion *);
unsigned char			vm_xor(t_champion *);

/*
** ins_registers.c
*/
unsigned char			ld(t_champion *);
unsigned char			st(t_champion *);
unsigned char			ldi(t_champion *);
unsigned char			sti(t_champion *);

#endif /* !MY_VM_H_ */
