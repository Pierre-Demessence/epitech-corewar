/*
** read_files.c for corewar in /home/taieb_t/rendu/Corewar
** 
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
** 
** Started on  Tue Dec  3 15:24:23 2013 taieb_t$
** Last update Sun Dec 15 20:20:33 2013 taieb_t$
*/

#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "my_vm.h"

header_t		*get_my_head(int fd, char *arg)
{
  header_t		*my_head;
  char			*buffer;

  if ((buffer = my_malloc(sizeof(header_t))) == NULL)
    return (NULL);
  if ((read(fd, buffer, sizeof(header_t))) == -1)
    return (NULL);
  my_head = (header_t *)buffer;
  my_revnchar(&(my_head->magic), sizeof(int));
  my_revnchar(&(my_head->prog_size), sizeof(int));
  if (my_head->magic != COREWAR_EXEC_MAGIC)
    {
      my_put_error_files(arg);
      return (NULL);
    }
  return (my_head);
}

char		*get_all_cmd(int fd, int *max)
{
  char		buff[128];
  int		res;
  char		*prog;
  int		size;

  prog = NULL;
  res = 42;
  size = 0;
  while (res != 0)
    {
      res = read(fd, buff, 128);
      prog = my_dupstrcat(prog, buff, res, size);
      size = size + res;
      *max = *max + res;
    }
  return (prog);
}

char		init_my_champi(t_champion **champi, header_t *header)
{
  if (((*champi)->name = my_strdup(header->prog_name)) == NULL)
    return (1);
  if (((*champi)->comment = my_strdup(header->comment)) == NULL)
    return (1);
  (*champi)->life = 1;
  (*champi)->nb_cycle_to_wait = 0;
  (*champi)->action = op_tab[16];
  (*champi)->carry = 0;
  return (0);
}

t_champion	*get_my_champ(char *arg, t_buffer *buffer)
{
  t_champion	*champi;
  header_t	*header;
  int		fd;

  if ((champi = my_malloc(sizeof(t_champion))) == NULL)
    return (NULL);
  if ((fd = open(arg, O_RDONLY)) == -1)
    {
      my_put_error_files(arg);
      return (NULL);
    }
  if ((header = get_my_head(fd, arg)) == NULL)
    return (NULL);
  if (init_my_champi(&champi, header))
    return (NULL);
  buffer->size = 0;
  buffer->buffer = get_all_cmd(fd, &(buffer->size));
  if (buffer->size != header->prog_size)
    {
      my_put_error_files(arg);
      return (NULL);
    }
  free(header);
  (void)close(fd);
  return (champi);
}

t_buffer	**init_all_champ_buffer(t_char_list *paths,
					t_list **all_champs)
{
  t_buffer	**buffers;
  t_champion	*my_champion;
  int		count;

  if ((buffers = my_malloc((nb_elem_in_path_list(paths) + 1) *
			sizeof(t_buffer *))) == NULL)
    return (NULL);
  count = 0;
  while (paths != NULL)
    {
      if ((buffers[count] = my_malloc(sizeof(t_buffer))) == NULL)
	return (NULL);
      if ((my_champion = get_my_champ(paths->str, buffers[count])) == NULL)
	return (NULL);
      if (my_put_in_list(all_champs, my_champion))
	return (NULL);
      count = count + 1;
      paths = paths->next;
    }
  buffers[count] = NULL;
  return (buffers);
}

