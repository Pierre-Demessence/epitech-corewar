/*
** champions_functions_tools.c for corewar in /home/taieb_t/Desktop/corewar/VM
** 
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
** 
** Started on  Fri Dec  6 10:14:32 2013 taieb_t$
** Last update Sun Dec 15 20:16:17 2013 taieb_t$
*/

#include <unistd.h>
#include <stdlib.h>
#include "my_vm.h"

int		are_you_alive(t_champion *champion)
{
  if (champion->life <= 0)
    {
      champion->life = -1;
      return (0);
    }
  else
    {
      champion->life = 0;
      return (1);
    }
}

int		my_check_corpses(t_list *champions)
{
  int		i;

  i = 0;
  while (champions != NULL)
    {
      i = i + are_you_alive(champions->champion);
      champions = champions->next;
    }
  return (i);
}

void		copy_registers(t_champion *new, t_champion *origin)
{
  int		i;

  i = 0;
  while (i < REG_NUMBER)
    {
      new->registers[i] = origin->registers[i];
      i = i + 1;
    }
}

t_champion	*copy_champion(t_champion *champi)
{
  t_champion	*new_champ;

  if ((new_champ = my_malloc(sizeof(t_champion))) == NULL)
    exit(1);
  if ((new_champ->name = my_strdup(champi->name)) == NULL)
    exit(1);
  if ((new_champ->comment = my_strdup(champi->comment)) == NULL)
    exit(1);
  new_champ->num_player = champi->num_player;
  new_champ->carry = champi->carry;
  new_champ->nb_cycle_to_wait = 0;
  new_champ->action = op_tab[16];
  new_champ->pc = 0;
  new_champ->life = champi->life;
  new_champ->alive = 0;
  copy_registers(new_champ, champi);
  if (my_put_in_list(&g_champions, new_champ))
    exit(1);
  return (new_champ);
}
