/*
** set_number_players.c for corewar in /home/taieb_t/Desktop/corewar/VM
** 
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
** 
** Started on  Mon Dec  9 21:59:03 2013 taieb_t$
** Last update Sat Dec 14 19:53:42 2013 taieb_t$
*/

#include <stdlib.h>
#include "my_vm.h"

char		check_number_players(t_options *opts, int size)
{
  int		i;
  int		j;

  i = -1;
  while (++i < size)
    {
      j = -1;
      if (opts->numbers[i] == 0 && opts->numbers[i] > MAX_NUM_PLAYER)
	{
	  show_usage();
	  return (1);
	}
      while (++j < size)
	{
	  if (i != j && opts->numbers[i] != -1 &&
	      opts->numbers[i] == opts->numbers[j])
	    {
	      my_put_errstr("prog number [");
	      my_put_nbr(opts->numbers[i], 2);
	      my_put_errstr("] alreeady used\n");
	      return (1);
	    }
	}
    }
  return (0);
}

char		exist_in_my_tab(int nb, t_options *opts, int size)
{
  int		i;

  i = 0;
  while (i < size)
    {
      if (opts->numbers[i] == nb)
	return (1);
      i = i + 1;
    }
  return (0);
}

void		set_num_by_options(t_list *champions,
				   t_options *opts, int size)
{
  int		i;

  i = size - 1;
  while (champions != NULL)
    {
      if (opts->numbers[i] != -1)
	champions->champion->num_player = opts->numbers[i];
      i = i - 1;
      champions = champions->next;
    }
}

char		set_number_player(t_list *champions, t_options *opts)
{
  int		i;
  int		size;
  int		count_nb;

  size = nb_elem_in_list(champions);
  if (check_number_players(opts, size))
    return (1);
  set_num_by_options(champions, opts, size);
  count_nb = 1;
  i = size - 1;
  while (champions != NULL)
    {
      if (opts->numbers[i] == -1)
	{
	  while (exist_in_my_tab(count_nb, opts, size))
	    count_nb = count_nb + 1;
	  champions->champion->num_player = count_nb;
	  count_nb = count_nb + 1;
	}
      champions = champions->next;
      i = i - 1;
    }
  return (0);
}
