/*
** ins_base.c for Corewar/VM in /home/demess_p/Projects/C/Corewar/VM
** 
** Made by demess_p
** Login   <demess_p@epitech.net>
** 
** Started on  Mon Dec  2 19:37:51 2013 demess_p
** Last update Sun Dec 15 18:50:00 2013 demess_p
*/

#include <unistd.h>
#include "mem_utils.h"
#include "my_vm.h"
#include "parm_utils.h"

unsigned char	live(t_champion *c_cur)
{
  t_list	*tmp;
  int		id;
  char		mess;

  id = mem_readi(4, c_cur->pc + 1);
  tmp = g_champions;
  mess = 0;
  while (tmp && tmp->champion)
    {
      if (tmp->champion->num_player == id)
	{
	  if (!mess && (mess = 1))
	    {
	      show_alive_champi(tmp->champion);
	      ++g_scheduler.total_lives;
	    }
	  ++tmp->champion->life;
	  g_winner = tmp->champion;
	}
      tmp = tmp->next;
    }
  return (1 + 4);
}

unsigned char	zjump(t_champion *c)
{
  int		idx;

  if (c->carry)
    {
      idx = mem_readi(2, c->pc + 1);
      c->pc = (c->pc + idx % IDX_MOD) % MEM_SIZE;
      return (0);
    }
  return (1 + 2);
}

unsigned char	aff(t_champion *c_cur)
{
  const t_parms	*parms;
  char		c;

  parms = get_parms(c_cur, 0);
  if (!parms->error)
    {
      c = parms->p[0] % 256;
      (void)write(1, &c, 1);
    }
  return (1 + 1 + parms->size);
}

unsigned char	vm_fork(t_champion *c_cur)
{
  unsigned int	idx;
  t_champion	*c_new;

  idx = mem_readi(2, c_cur->pc + 1);
  c_new = copy_champion(c_cur);
  c_new->pc = (c_cur->pc + idx % IDX_MOD) % MEM_SIZE;
  return (1 + 2);
}
