.name			"Le semi-croustillant"
.comment		"C'est pas faux !"

input:	and %0, %0, r2
	zjmp %:iner

b1:	and %0, %0, r3
	zjmp %:b1

b2:	and %0, %0, r3
	zjmp %:b1

b3:	and %0, %0, r3
	zjmp %:b1

b4:	and %0, %0, r3
	zjmp %:b1

b5:	and %0, %0, r3
	zjmp %:b1

b6:	and %0, %0, r3
	zjmp %:b1

b7:	and %0, %0, r3
	zjmp %:b1

b8:	and %0, %0, r3
	zjmp %:b1

b9:	and %0, %0, r3
	zjmp %:b1

b10:	and %0, %0, r3
	zjmp %:b1

b11:	and %0, %0, r3
	zjmp %:b1

b12:	and %0, %0, r3
	zjmp %:b1

b13:	and %0, %0, r3
	zjmp %:b1
	
b14:	and %0, %0, r3
	zjmp %:b1

iner:	sti r5, %:input, %1
	sti r5, %:input, %2
	sti r5, %:input, %3
	sti r5, %:input, %4
	sti r5, %:input, %5
	sti r5, %:input, %6
	sti r5, %:input, %7
	sti r5, %:input, %8
	sti r5, %:input, %9
	sti r1, %:liveme, %1
	
liveme:	live %1
	fork %:entree
	and %0, %0, r2
	zjmp %:azerty
	
entree:	sti r1, %:live, %1
live:	live %1
	and %0, %0, r2
	zjmp %:entree

azerty:	fork %:entree
	sti r1, %:live, %1
	live %1
	fork %:azerty
	sti r1, %:live, %1
	live %1
	and %0, %0, r2
	zjmp %:azerty

b15:	and %0, %0, r3
	zjmp %:b15

b16:	and %0, %0, r3
	zjmp %:b15

b17:	and %0, %0, r3
	zjmp %:b15

b18:	and %0, %0, r3
	zjmp %:b15

b18:	and %0, %0, r3
	zjmp %:b15

b19:	and %0, %0, r3
	zjmp %:b15

b20:	and %0, %0, r3
	zjmp %:b15

b21:	and %0, %0, r3
	zjmp %:b15
