/*
** scheduling.c for corewar in /home/taieb_t/rendu/Corewar
** 
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
** 
** Started on  Tue Dec  3 19:18:32 2013 taieb_t$
** Last update Sun Dec 15 22:30:21 2013 taieb_t$
*/

#include <stdlib.h>
#include <unistd.h>
#include "my_vm.h"
#include "sdl.h"

t_champion		*g_winner;
t_scheduling		g_scheduler;
static unsigned char	((*g_fptr[])(t_champion *)) =
{
  live,
  ld,
  st,
  vm_add,
  vm_sub,
  vm_and,
  vm_or,
  vm_xor,
  zjump,
  ldi,
  sti,
  vm_fork,
  lld,
  lldi,
  lfork,
  aff,
  NULL
};

void		do_action(t_champion *champion, t_options *options, op_t action)
{
  unsigned char	pc_move;

  if (action.code != champion->action.code)
    {
      champion->action = action;
      champion->nb_cycle_to_wait = action.nbr_cycles;
    }
  champion->nb_cycle_to_wait = champion->nb_cycle_to_wait - 1;
  my_show_action_loading(champion, options);
  if (champion->nb_cycle_to_wait <= 0)
    {
      my_show_action(champion, options);
      if (champion->action.code >= 1 && champion->action.code <= 16 &&
	  g_fptr[champion->action.code - 1] != NULL)
	{
	  if ((pc_move = g_fptr[(int)(champion->action.code - 1)](champion)) == 1)
	    if ((options->options_basic & 0x08) == 0x08)
	      my_putstr("It's not very effective...\n");
	}
      else
	pc_move = 1;
      my_show_pc(champion, options);
      champion->pc = ((champion->pc + pc_move) % MEM_SIZE);
    }
}

void		exec_champions_actions(t_list *champions, t_options *options,
				       t_scheduling *schedule)
{
  int		i;

  while (champions != NULL)
    {
      if (champions->champion->life != -1)
	{
	  i = exist_in_op_tab(get_action_to_do(champions->champion));
	  do_action(champions->champion, options, op_tab[i]);
	}
      champions = champions->next;
    }
  by_step_function(options, schedule);
}

void		check_alive_champion(t_scheduling *schedule, int flag)
{
  static int	count = 0;

  if ((get_nb_alive() >= flag) &&
      ((MAX_CYCLES > 0) && count >= MAX_CYCLES))
    {
      schedule->cycles_to_die = schedule->cycles_to_die - CYCLE_DELTA;
      count = 0;
      set_flag_alive(1);
    }
  count = count + 1;
}

char		exec_my_cycles(t_list *champions, t_options *options,
			       t_scheduling *schedule)
{
  int		cycles;
  int		flag;

  cycles = 0;
  flag = get_nb_alive();
  while (cycles < schedule->cycles_to_die)
    {
#ifdef BONUS
      if (((options->options_basic & 0x20) == 0x20) && schedule->aff_graphics)
	sdl_draw(schedule);
#endif
      exec_champions_actions(champions, options, schedule);
      cycles = cycles + 1;
      schedule->total_cycles = schedule->total_cycles + 1;
      if (schedule->total_lives >= NBR_LIVE && !(schedule->total_lives = 0))
	check_alive_champion(schedule, flag);
      if ((options->options_advanced & 0x01) == 1 &&
	  schedule->total_cycles >= options->values[0])
	{
	  show_memory();
	  return (1);
	}
    }
  check_alive_champion(schedule, flag);
  return (0);
}

void		scheduler(t_options *options, int graphics)
{
  int		res;

  g_scheduler.cycles_to_die = CYCLE_TO_DIE;
  g_scheduler.total_cycles = 0;
  g_scheduler.total_lives = 0;
  g_scheduler.read_numbers = 0;
  g_scheduler.aff_graphics = graphics;
  g_winner = NULL;
  set_flag_alive(0);
  while ((res = my_check_corpses(g_champions)) > 0)
    {
      if (exec_my_cycles(g_champions, options, &g_scheduler))
	return ;
    }
  my_show_winner(g_winner, options);
#ifdef BONUS
  if (((options->options_basic & 0x20) == 0x20) && g_scheduler.aff_graphics)
    free_my_sdl();
#endif
}
