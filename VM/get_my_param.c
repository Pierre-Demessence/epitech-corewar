/*
** get_my_param.c for corewar in /home/taieb_t/Desktop/corewar/VM
** 
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
** 
** Started on  Mon Dec  9 16:02:08 2013 taieb_t$
** Last update Sun Dec 15 20:40:45 2013 taieb_t$
*/

#include <unistd.h>
#include <stdlib.h>
#include "my_vm.h"

static char	*g_options_number[6] =
  {
    "-dump",
    "-pourcent",
    "-ctmo",
    "-noX",
    "-n",
    "-a"
  };

static char	*g_options_basic[6] =
  {
    "-comment",
    "-by_step",
    "-aff",
    "-aff_shell",
    "-aff_proc",
    "-graphics"
  };

int		check_in_options(char *str)
{
  int		i;

  i = 0;
  while (i < 6)
    {
      if (my_strcmp(str, g_options_number[i++]) == 0)
	return (2);
    }
  i = 0;
  while (i < 6)
    {
      if (my_strcmp(str, g_options_basic[i++]) == 0)
	return (1);
    }
  return (0);
}

void		my_char_add_list_to_list(t_char_list **begin1, t_char_list *begin2)
{
  t_char_list	*memo;

  memo = *begin1;
  if ((*begin1) == NULL)
    {
      (*begin1) = begin2;
      return ;
    }
  while ((*begin1)->next != NULL)
    *begin1 = (*begin1)->next;
  (*begin1)->next = begin2;
  *begin1 = memo;
  return ;
}

int		my_put_in_char_list(t_char_list **list, char *param)
{
  t_char_list	*elem;

  elem = my_malloc(sizeof(t_char_list));
  if (elem == NULL)
    return (1);
  elem->str = param;
  elem->next = NULL;
  my_char_add_list_to_list(list, elem);
  return (0);
}

t_char_list	*get_my_champi(int ac, char **av)
{
  int		i;
  int		check;
  int		previous;
  t_char_list	*res;

  i = 0;
  res = NULL;
  previous = -1;
  while (++i < ac)
    {
      check = check_in_options(av[i]);
      if (!check)
	my_put_in_char_list(&res, av[i]);
      if (previous == 2 && check != 0)
      	{
      	  show_usage();
      	  return (NULL);
      	}
      else if (check == 2 && (check = 1))
	i = i + 1;
      previous = check;
    }
  return (res);
}

t_char_list	*get_my_options(int ac, char **av)
{
  int		i;
  int		check;
  t_char_list	*res;

  i = 0;
  res = NULL;
  while (++i < ac)
    {
      if ((check = check_in_options(av[i])) == 1)
	my_put_in_char_list(&res, av[i]);
      else if (check == 2)
	{
	  my_put_in_char_list(&res, av[i++]);
	  if (i < ac && my_is_num_str(av[i]) == 0)
	    my_put_in_char_list(&res, av[i]);
	  else
	    {
	      show_usage();
	      return (NULL);
	    }
	}
      else
	my_put_in_char_list(&res, "champi");
    }
  return (res);
}
