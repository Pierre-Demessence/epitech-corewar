/*
** mem_utils.h for Corewar/VM in /home/demess_p/Projects/C/Corewar/VM
** 
** Made by demess_p
** Login   <demess_p@epitech.net>
** 
** Started on  Fri Dec  6 12:40:37 2013 demess_p
** Last update Thu Dec 12 17:06:49 2013 demess_p
*/

#ifndef MEM_UTILS_H_
# define MEM_UTILS_H_

char	*mem_read(int nb, int idx_start);
int	mem_readi(int nb, int idx_start);
void	mem_write(int nb, int idx_start, char *data, int num_player);

#endif /* !MEM_UTILS_H_ */
