/*
** options_schedule.c for corewar in /home/taieb_t/Desktop/corewar/VM
** 
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
** 
** Started on  Mon Dec  9 22:55:26 2013 taieb_t$
** Last update Sun Dec 15 21:46:12 2013 taieb_t$
*/

#include <unistd.h>
#include "my_vm.h"
#include "sdl.h"

void		my_show_action(t_champion *champion,
			       t_options *options)
{
  if ((options->options_basic & 0x08) == 0x08)
    {
      if (champion->action.code == 0)
	{
	  my_putstr("[");
	  my_putstr(champion->name);
	  my_putstr("] do nothing\n");
	}
      else
	{
	  my_putstr("[");
	  my_putstr(champion->name);
	  my_putstr("] do ");
	  my_putstr(champion->action.mnemonique);
	  my_putstr("\n");
	}
    }
}

void		my_show_action_loading(t_champion *champion,
				       t_options *options)
{
  if ((options->options_basic & 0x04) == 0x04)
    {
      if (champion->action.code == 0)
	{
	  my_putstr("[");
	  my_putstr(champion->name);
	  my_putstr("] is doing nothing\n");
	}
      else
	{
	  my_putstr("[");
	  my_putstr(champion->name);
	  my_putstr("] is doing [");
	  my_putstr(champion->action.mnemonique);
	  my_putstr("]\n");
	}
    }
}

void		my_show_pc(t_champion *champion,
			   t_options *options)
{
  if ((options->options_basic & 0x10) == 0x10)
    {
      my_putstr("[");
      my_putstr(champion->name);
      my_putstr("] pc => [");
      if (champion->pc != 0)
	my_put_nbr(champion->pc, 1);
      else
	(void)write(1, "0", 1);
      my_putstr("]\n");
    }
}

void		free_my_sdl()
{
  char		buff[1];

  my_putstr("Press enter to close the graphic\n");
  if (read(0, buff, 1) == -1)
    my_put_errstr("Read failed ...\n");
#ifdef BONUS
  (void)sdl_free();
#endif
}
