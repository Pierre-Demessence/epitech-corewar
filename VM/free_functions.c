/*
** free_functions.c for corewar in /home/taieb_t/rendu/Corewar
** 
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
** 
** Started on  Tue Dec  3 15:14:08 2013 taieb_t$
** Last update Thu Dec 12 18:13:07 2013 demess_p
*/

#include <stdlib.h>
#include "my_vm.h"

void	my_free_list(t_list *list)
{
  if (list == NULL)
    return ;
  my_free_list(list->next);
  if (list->champion != NULL)
    {
      if (list->champion->name != NULL)
  	free(list->champion->name);
      if (list->champion->comment != NULL)
  	free(list->champion->comment);
      free(list->champion);
    }
  free(list);
}

void	my_free_char_list(t_char_list *list)
{
  if (list == NULL)
    return ;
  my_free_char_list(list->next);
  free(list);
}

void	my_free_all(t_list *list, t_options *opt,
		    t_char_list *char_opt, t_char_list *char_champi)
{
  my_free_list(list);
  my_free_char_list(char_opt);
  my_free_char_list(char_champi);
  free(opt->numbers);
  free(opt->loader);
  free(opt);
}

void		free_my_champi(t_list *champions)
{
  free(champions->champion->name);
  free(champions->champion->comment);
  free(champions->champion);
  free(champions);
}

void	my_free_buffers(t_buffer **str, int max)
{
  int	i;

  i = 0;
  while (i < max)
    {
      if (str[i] != NULL)
	{
	  free(str[i]->buffer);
	  free(str[i]);
	}
      i = i + 1;
    }
  free(str);
}
