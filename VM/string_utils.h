/*
** string_utils.h for Corewar/VM in /home/demess_p/Projects/C/Corewar/VM
** 
** Made by demess_p
** Login   <demess_p@epitech.net>
** 
** Started on  Sat Dec 14 18:32:33 2013 demess_p
** Last update Sat Dec 14 18:37:37 2013 demess_p
*/

#ifndef STRING_UTILS_H_
# define STRING_UTILS_H_

char	*my_strconcat(char *s1, char *s2);
int	my_nbrlen(long int nbr, char *base);
char	*my_nbrtostr(long int nbr, char *base);

#endif /* !STRING_UTILS_H_ */
