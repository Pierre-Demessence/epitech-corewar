/*
** scheduling_tools.c for corewar in /home/taieb_t/Desktop/corewar/VM
** 
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
** 
** Started on  Mon Dec  9 18:22:17 2013 taieb_t$
** Last update Sun Dec 15 16:56:04 2013 taieb_t$
*/

#include <stdlib.h>
#include <unistd.h>
#include "my_vm.h"

extern char	memory[MEM_SIZE];

void		my_show_winner(t_champion *winner, t_options *options)
{
  if (winner == NULL)
    my_put_errstr("nobody won ...\n");
  else if ((options->options_basic & 0x01) == 1)
    {
      my_putstr("le joueur ");
      my_put_nbr(winner->num_player, 1);
      my_putstr("(");
      my_putstr(winner->name);
      my_putstr(") a gagné : ");
      my_putstr(winner->comment);
      my_putstr("\n");
    }
  else
    {
      my_putstr("le joueur ");
      my_put_nbr(winner->num_player, 1);
      my_putstr("(");
      my_putstr(winner->name);
      my_putstr(") a gagné\n");
    }
}

int		get_action_to_do(t_champion *champion)
{
  int		pos;

  pos = 0;
  if (champion != NULL)
    pos = g_memory[(champion->pc % MEM_SIZE)];
  return (pos);
}

int		exist_in_op_tab(int action)
{
  int		i;

  i = 0;
  while (op_tab[i].code != 0)
    {
      if (action == op_tab[i].code)
	return (i);
      i = i + 1;
    }
  return (i);
}

void		read_my_buffer(t_scheduling *schedule, int res, char buffer[12])
{
  if (res == 1)
    schedule->read_numbers = 1;
  else if (!my_is_num_str(buffer))
    schedule->read_numbers = my_getnbr(buffer);
  else if (my_strcmp("dump", buffer) == 0)
    show_memory();
  else
    my_put_errstr("Not readable infos...\n");
}

void		by_step_function(t_options *options, t_scheduling *schedule)
{
  char		buffer[12];
  int		res;

  if ((options->options_basic & 0x02) == 0x02)
    {
      schedule->read_numbers = schedule->read_numbers - 1;
      if (schedule->read_numbers <= 0)
	{
	  if ((res = read(0, buffer, 11)) == -1)
	    {
	      my_put_errstr("Read failed ...\n");
	      options->options_basic = options->options_basic - 0x02;
	    }
	  else
	    {
	      buffer[res - 1] = 0;
	      read_my_buffer(schedule, res, buffer);
	    }
	}
    }
}
