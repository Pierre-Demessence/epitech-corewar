/*
** string_utils.c for Corewar/VM in /home/demess_p/Projects/C/Corewar/VM
** 
** Made by demess_p
** Login   <demess_p@epitech.net>
** 
** Started on  Sat Dec 14 18:25:03 2013 demess_p
** Last update Sun Dec 15 20:13:14 2013 demess_p
*/

#include <stdlib.h>
#include "my_vm.h"

char	*my_strconcat(char *s1, char *s2)
{
  char	*res;
  int	i;

  res = my_malloc(my_strlen(s1) + my_strlen(s2) + 1);
  if (!res)
    exit(EXIT_FAILURE);
  i = 0;
  while (s1 && *s1)
    {
      res[i] = *s1;
      ++i;
      ++s1;
    }
    while (s2 && *s2)
    {
      res[i] = *s2;
      ++i;
      ++s2;
    }
  res[i] = '\0';
  return (res);
}

int	my_nbrlen(long int nbr, char *base)
{
  int	blen;

  blen = my_strlen(base);
  if (nbr > (blen - 1) || nbr < -1 * (blen - 1))
    return (1 + my_nbrlen(nbr / blen, base));
  return (1);
}

char	*my_nbrtostr(long int nbr, char *base)
{
  int	i;
  int	len;
  char	*res;
  char	sign;
  int	blen;

  blen = my_strlen(base);
  len = my_nbrlen(nbr, base);
  sign = (nbr < 0 ? 1 : 0);
  res = my_malloc((len + sign + 1) * sizeof(char));
  if (!res)
    exit(EXIT_FAILURE);
  nbr *= (sign ? -1 : 1);
  res[len + sign - 1] = base[(nbr % blen)];
  i = len + sign - 2;
  while (i >= (sign) && (nbr /= blen))
    res[i--] = base[(nbr % blen)];
  res[len + sign] = '\0';
  if (sign)
    res[0] = '-';
  return (res);
}
