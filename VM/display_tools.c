/*
** show_usage.c for corewar in /home/taieb_t/Desktop/corewar/VM
** 
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
** 
** Started on  Mon Dec  9 16:30:31 2013 taieb_t$
** Last update Sun Dec 15 12:05:20 2013 taieb_t$
*/

#include <unistd.h>
#include "my_vm.h"

void	my_putstr(char *str)
{
  (void)write(1, str, my_strlen(str));
}

void	my_put_errstr(char *str)
{
  (void)write(2, str, my_strlen(str));
}

void	my_put_error_files(char * str)
{
  my_put_errstr("File ");
  my_put_errstr(str);
  my_put_errstr(" is not a good .cor\n");
}

void	my_put_nbr(int nb, int fd)
{
  int	tempo;
  char	c;

  if (nb == 0)
    return ;
  tempo = nb % 10;
  c = tempo + '0';
  nb = nb / 10;
  my_put_nbr(nb, fd);
  (void)write(fd, &c, 1);
}

char	show_usage()
{
  my_putstr("Usage : ./corewar [-comment] [-by_step]\n");
  my_putstr("	[-aff] [-aff_shell] [-aff_proc]\n");
  my_putstr("	[-dump nbr_cycle] [-graphics]\n");
  my_putstr("	[[-n prog_number] [-a load_address] prog_name] ...\n");
  return (1);
}
