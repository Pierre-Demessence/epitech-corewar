/*
** functions_sys.c for corewar in /home/taieb_t/Desktop/corewar/VM
** 
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
** 
** Started on  Sun Dec 15 20:13:21 2013 taieb_t$
** Last update Sun Dec 15 20:14:31 2013 taieb_t$
*/

#include <stdlib.h>
#include "my_vm.h"

void	*my_malloc(unsigned int size)
{
  void	*res;

  res = malloc(size);
  if (res == NULL)
    my_put_errstr("Can't perform malloc\n");
  return (res);
}
