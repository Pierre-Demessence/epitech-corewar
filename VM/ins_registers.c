/*
** ins_registers.c for Corewar/VM in /home/demess_p/Projects/C/Corewar/VM
** 
** Made by demess_p
** Login   <demess_p@epitech.net>
** 
** Started on  Mon Dec  2 19:42:49 2013 demess_p
** Last update Sat Dec 14 18:04:36 2013 demess_p
*/

#include "mem_utils.h"
#include "my_vm.h"
#include "parm_utils.h"

unsigned char	ld(t_champion *c)
{
  const t_parms	*parms;

  parms = get_parms(c, PARM2_IDX);
  if (!parms->error)
    {
      c->carry = (parms->p[0] == 0 ? 1 : 0);
      c->registers[parms->p[1]] = parms->p[0];
    }
  return (1 + 1 + parms->size);
}

unsigned char	st(t_champion *c)
{
  const t_parms	*parms;

  parms = get_parms(c, PARM2_IDX);
  if (!parms->error)
    {
      if (parms->t[1] == T_REG)
	c->registers[parms->p[1]] = parms->p[0];
      else
	mem_write(REG_SIZE, parms->p[1], ((char*)(parms->p)), c->num_player);
    }
  return (1 + 1 + parms->size);
}

unsigned char	ldi(t_champion *c)
{
  const t_parms	*parms;
  int		value;

  parms = get_parms(c, PARM3_IDX | PARM_IDXS);
  if (!parms->error)
    {
      value = mem_readi(REG_SIZE,
			c->pc + (parms->p[0] + parms->p[1]) % IDX_MOD);
      c->carry = (value == 0 ? 1 : 0);
      c->registers[parms->p[2]] = value;
    }
  return (1 + 1 + parms->size);
}

unsigned char	sti(t_champion *c)
{
  const t_parms	*parms;
  int		idx;

  parms = get_parms(c, PARM_IDXS);
  if (!parms->error)
    {
      idx = parms->p[1] + parms->p[2];
      mem_write(REG_SIZE, c->pc + idx % IDX_MOD,
		((char*)(parms->p)), c->num_player);
    }
  return (1 + 1 + parms->size);
}
