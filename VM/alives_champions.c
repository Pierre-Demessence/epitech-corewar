/*
** alives_champions.c for corewar in /home/taieb_t/Desktop/corewar/VM
** 
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
** 
** Started on  Fri Dec 13 10:56:52 2013 taieb_t$
** Last update Sun Dec 15 17:46:47 2013 taieb_t$
*/

#include <stdlib.h>
#include "my_vm.h"

void		set_flag_alive(int flag)
{
  t_list	*memo;

  memo = g_champions;
  while (memo != NULL)
    {
      if (memo->champion->life != -1)
	memo->champion->alive = flag;
      memo = memo->next;
    }
  return ;
}

int		get_nb_alive()
{
  t_list	*memo;
  int		res;

  memo = g_champions;
  res = 0;
  while (memo != NULL)
    {
      if (memo->champion->life != -1 && memo->champion->alive == 1)
	res = res + 1;
      memo = memo->next;
    }
  return (0);
}

void		show_alive_champi(t_champion *alive_champi)
{
  my_putstr("Le joueur ");
  my_put_nbr(alive_champi->num_player, 1);
  my_putstr("(");
  my_putstr(alive_champi->name);
  my_putstr(") est en vie\n");
}
