/*
** show_memory.c for corewar in /home/taieb_t/rendu/Corewar
** 
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
** 
** Started on  Wed Dec  4 14:02:57 2013 taieb_t$
** Last update Sun Dec 15 17:51:53 2013 taieb_t$
*/

#include <unistd.h>
#include "my_vm.h"

long int	my_putnbr_base(long int nbr, char *base)
{
  int	length;
  int	tempo;

  if (nbr == 0)
    return (0);
  length = my_strlen(base);
  if (length <= 1)
    return (0);
  tempo = nbr % length;
  nbr = nbr / length;
  if (nbr < 0)
    nbr = nbr * -1;
  my_putnbr_base(nbr, base);
  (void)write(1, &(base[tempo]), 1);
  return (0);
}

void	printer_char(char *str)
{
  if ((unsigned char)*str == 0)
    {
      (void)write(1, "00", 2);
      return ;
    }
  else if ((unsigned char)*str < 16)
    (void)write(1, "0", 1);
  my_putnbr_base((unsigned char)*str, "0123456789abcdef");
}

int	my_print_hexa_values(char *str, int count, int size)
{
  int	i;

  i = 0;
  while (i < 16 && count < size)
    {
      if (count >= size)
	break;
      printer_char(str);
      (void)write(1, " ", 1);
      i = i + 1;
      str = str + 1;
      count = count + 1;
    }
  str = str - i;
  return (count);
}

int	my_showmem(char *str, int size)
{
  int	memory_add;
  int	count;

  memory_add = 0;
  count = 0;
  (void)write(1, "0", 1);
  while (count < size)
    {
      if (memory_add < 16)
	(void)write(1, "0", 1);
      if (memory_add < 256)
	(void)write(1, "0", 1);
      if (memory_add < 4096)
	(void)write(1, "0", 1);
      my_putnbr_base(memory_add, "0123456789abcdef");
      (void)write(1, " : ", 3);
      count = my_print_hexa_values(str, count, size);
      (void)write(1, "\n", 1);
      str = str + 16;
      memory_add = memory_add + 16;
    }
  return (0);
}

void		show_memory()
{
  my_showmem(g_memory, MEM_SIZE);
}
