/*
** convert_to_big_endian.c for corewar in /home/taieb_t/rendu/Corewar
** 
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
** 
** Started on  Wed Dec  4 18:45:03 2013 taieb_t$
** Last update Fri Dec 13 22:58:18 2013 taieb_t$
*/

void	my_revnchar(void *ptr, int nbr)
{
  int	i;
  int	j;
  char	c;

  i = 0;
  if (nbr <= 1)
    return ;
  j = nbr - 1;
  while (i <= j)
    {
      c = ((char *)ptr)[i];
      ((char *)ptr)[i] = ((char *)ptr)[j];
      ((char *)ptr)[j] = c;
      i = i + 1;
      j = j - 1;
    }
}
