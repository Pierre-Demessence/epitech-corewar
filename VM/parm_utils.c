/*
** parm_utils.c for Corewar/VM in /home/demess_p/Projects/C/Corewar/VM
** 
** Made by demess_p
** Login   <demess_p@epitech.net>
** 
** Started on  Thu Dec  5 16:42:37 2013 demess_p
** Last update Sat Dec 14 17:59:05 2013 demess_p
*/

#include <stdlib.h>
#include "mem_utils.h"
#include "my_vm.h"
#include "op.h"
#include "parm_utils.h"

void	tparms_init(t_parms *parms)
{
  int	i;

  parms->error = 0;
  parms->offset = 0;
  parms->size = 0;
  i = 0;
  while (i < MAX_ARGS_NUMBER)
    {
      parms->p[i] = 0;
      parms->t[i] = 0;
      parms->s[i] = 0;
      ++i;
    }
}

void			tparms_set_types(t_parms *parms, op_t ins,
					 char coding, char idx_size)
{
  char			mask;
  int			i;
  static const int      sizes[] = {0, 1, DIR_SIZE, 0, IND_SIZE};

  i = 0;
  while (i < ins.nbr_args && !parms->error)
    {
      mask = (coding >> ((MAX_ARGS_NUMBER - 1 - i) * 2)) & 0x03;
      if (mask == 0x01)
	parms->t[i] = T_REG;
      else if (mask == 0x02)
	parms->t[i] = T_DIR;
      else if (mask == 0x03)
	parms->t[i] = T_IND;
      else
	parms->t[i] = 0;
      parms->s[i] = (parms->t[i] != T_REG && idx_size ?
		     IND_SIZE :
		     sizes[(int)parms->t[i]]);
      parms->size += parms->s[i];
      if (!parms->t[i] || !(ins.type[i] & parms->t[i]))
	parms->error = 1;
      ++i;
    }
}

void	tparms_set_parm(t_parms *parms, t_champion *c, char flags, int n)
{
  int	index;

  if (parms->t[n] == T_REG )
    {
      index = (mem_readi(parms->s[n], c->pc + parms->offset)) - 1;
      if ((index < 0 || index >= REG_NUMBER) && (parms->error = 1))
	return ;
      if (flags & FLAG_IDX)
	parms->p[n] = index;
      else
	parms->p[n] = c->registers[index];
    }
  else if (parms->t[n] == T_IND)
    if (flags & FLAG_IDX)
      parms->p[n] = mem_readi(parms->s[n], c->pc + parms->offset);
    else
      if (flags & FLAG_LNG)
	parms->p[n] = mem_readi(REG_SIZE, c->pc +
				mem_readi(parms->s[n], c->pc + parms->offset));
      else
	parms->p[n] = mem_readi(REG_SIZE, c->pc +
				mem_readi(parms->s[n], c->pc + parms->offset) %
				IDX_MOD);
  else if (parms->t[n] == T_DIR)
    parms->p[n] = mem_readi(parms->s[n], c->pc + parms->offset);
}

void	tparms_set_parms(t_parms *parms, t_champion *c, char flags)
{
  char	flag;
  int	i;

  parms->offset = 2;
  i = 0;
  while (i < MAX_ARGS_NUMBER && !parms->error)
    {
      flag = 0;
      if ((flags >> i) & 1)
	flag |= FLAG_IDX;
      if (flags & PARM_LONG)
	flag |= FLAG_LNG;
      tparms_set_parm(parms, c, flag, i);
      parms->offset += parms->s[i];
      ++i;
    }
}

const t_parms		*get_parms(t_champion *c, char flags)
{
  static t_parms	parms;
  char			*codage;

  tparms_init(&parms);
  codage = mem_read(1, c->pc + 1);
  tparms_set_types(&parms,
		   op_tab[(int)g_memory[c->pc] - 1],
		   codage[0],
		   flags & PARM_IDXS);
  free(codage);
  if (parms.error)
    {
      parms.size = -1;
      return (&parms);
    }
  tparms_set_parms(&parms, c, flags);
  return (&parms);
}
