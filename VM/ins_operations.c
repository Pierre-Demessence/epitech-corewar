/*
** ins_operations.c for Corewar/VM in /home/demess_p/Projects/C/Corewar/VM
** 
** Made by demess_p
** Login   <demess_p@epitech.net>
** 
** Started on  Mon Dec  2 19:32:29 2013 demess_p
** Last update Sun Dec 15 16:40:09 2013 demess_p
*/

#include "mem_utils.h"
#include "my_vm.h"
#include "parm_utils.h"

unsigned char	vm_add(t_champion *c)
{
  const t_parms	*parms;

  parms = get_parms(c, PARM3_IDX);
  if (!parms->error)
    {
      c->carry = ((parms->p[0] + parms->p[1]) == 0 ? 1 : 0);
      c->registers[parms->p[2]] = parms->p[0] + parms->p[1];
    }
  return (1 + 1 + parms->size);
}

unsigned char	vm_sub(t_champion *c)
{
  const t_parms	*parms;

  parms = get_parms(c, PARM3_IDX);
  if (!parms->error)
    {
      c->carry = ((parms->p[0] - parms->p[1]) == 0 ? 1 : 0);
      c->registers[parms->p[2]] = parms->p[0] - parms->p[1];
    }
  return (1 + 1 + parms->size);
}

unsigned char	vm_and(t_champion *c)
{
  const t_parms	*parms;

  parms = get_parms(c, PARM3_IDX);
  if (!parms->error)
    {
      c->carry = ((parms->p[0] & parms->p[1]) == 0 ? 1 : 0);
      c->registers[parms->p[2]] = parms->p[0] & parms->p[1];
    }
  return (1 + 1 + parms->size);
}

unsigned char	vm_or(t_champion *c)
{
  const t_parms	*parms;

  parms = get_parms(c, PARM3_IDX);
  if (!parms->error)
    {
      c->carry = ((parms->p[0] | parms->p[1]) == 0 ? 1 : 0);
      c->registers[parms->p[2]] = parms->p[0] | parms->p[1];
    }
  return (1 + parms->size);
}

unsigned char	vm_xor(t_champion *c)
{
  const t_parms	*parms;

  parms = get_parms(c, PARM3_IDX);
  if (!parms->error)
    {
      c->carry = ((parms->p[0] ^ parms->p[1]) == 0 ? 1 : 0);
      c->registers[parms->p[2]] = parms->p[0] ^ parms->p[1];
    }
  return (1 + 1 + parms->size);
}
