/*
** mem_utils.c for Corewar/VM in /home/demess_p/Projects/C/Corewar/VM
** 
** Made by demess_p
** Login   <demess_p@epitech.net>
** 
** Started on  Tue Dec  3 16:48:46 2013 demess_p
** Last update Sun Dec 15 21:40:26 2013 taieb_t$
*/

#include <stdlib.h>
#include "my_vm.h"
#include "op.h"
#include "sdl.h"

char	*mem_read(int nb, int idx_start)
{
  char	*bytes;
  int	i;

  while (idx_start < 0)
    idx_start = MEM_SIZE - idx_start;
  if ((bytes = my_malloc(sizeof(char) * nb)) == NULL)
    exit(1);
  i = 0;
  while (i < nb)
    {
      bytes[i] = g_memory[(idx_start + (nb - 1 - i)) % MEM_SIZE];
      ++i;
    }
  return (bytes);
}

int	mem_readi(int nb, int idx_start)
{
  int	res;
  char	*bytes;

  bytes = mem_read(nb, idx_start);
  res = 0;
  if (nb == 1)
    res = *((char *)bytes);
  if (nb == 2)
    res = *((short *)bytes);
  if (nb == 4)
    res = *((int *)bytes);
  free(bytes);
  return (res);
}

void	mem_write(int nb, int idx_start, char *data, int num_player)
{
  int	i;

  while (idx_start < 0)
    idx_start = MEM_SIZE + idx_start;
  i = 0;
  while (i < nb)
    {
      g_memory[(idx_start + i) % MEM_SIZE] = data[nb - 1 - i];
#ifdef BONUS
      g_interface.cases[(idx_start + i) % MEM_SIZE] = num_player;
#endif
      ++i;
    }
  (void)num_player;
}
