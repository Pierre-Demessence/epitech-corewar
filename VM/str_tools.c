/*
** str_tools.c for corewar in /home/taieb_t/rendu/Corewar
** 
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
** 
** Started on  Tue Dec  3 15:16:49 2013 taieb_t$
** Last update Sun Dec 15 20:22:00 2013 taieb_t$
*/

#include <stdlib.h>
#include "my_vm.h"

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  if (str == NULL)
    return (0);
  while (str[i])
    i = i + 1;
  return (i);
}

char	*my_strdup(char *src)
{
  int	i;
  char	*dest;
  int	j;

  i = 0;
  while (src[i] != '\0')
    i = i + 1;
  j = 0;
  dest = my_malloc(i + 1);
  if (dest == NULL)
    return (NULL);
  while (j < i)
    {
      dest[j] = src[j];
      j = j + 1;
    }
  dest[j] = '\0';
  return (dest);
}

char	*my_dupstrcat(char *s1, char *s2, int size, int malloc_size)
{
  char	*res;
  int	pos;
  int	free_pos;
  int	count;

  if ((res = my_malloc(malloc_size + size + 1)) == NULL)
    return (NULL);
  pos = 0;
  free_pos = 0;
  while (free_pos < malloc_size)
    res[pos++] = s1[free_pos++];
  count = 0;
  while (count < size)
    res[pos++] = s2[count++];
  free(s1);
  res[pos] = 0;
  return (res);
}

int	my_strcmp(char *s1, char *s2)
{
  int	i;

  i = 0;
  while (*s1 != '\0' || *s2 != '\0')
    {
      i = *s1 - *s2;
      if (i != 0)
	return (i);
      s1 = s1 + 1;
      s2 = s2 + 1;
    }
  return (i);
}

int	my_is_num_str(char *str)
{
  while (*str)
    {
      if (*str < '0' || *str > '9')
	return (1);
      str = str + 1;
    }
  return (0);
}
