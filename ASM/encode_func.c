/*
** encode_func.c for corewar in /home/thing-_a/rendu/git_repo/epitech-corewar/ASM
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Tue Dec  3 19:25:38 2013 a
** Last update Sun Dec 15 23:09:20 2013 a
*/

#include <stdlib.h>
#include "op.h"
#include "asm.h"
#include "my.h"

extern char	error_msg;

char		*get_label_from_line(char *label)
{
  int		i;
  int		j;
  char		*label_tmp;

  i = 0;
  while (*label && (*label == ' ' || *label == '\t'))
    ++label;
  while (label[i])
    {
      j = 0;
      while (LABEL_CHARS[j] && label[i] != LABEL_CHARS[j])
	++j;
      if (!LABEL_CHARS[j])
	break ;
      ++i;
    }
  if (!(label_tmp = my_malloc(i + 1)))
    {
      error_msg = TRUE;
      return (NULL);
    }
  label_tmp = my_strncpy(label_tmp, label, i);
  return (label_tmp);
}

int		my_label_encode(int pc_addr, char *label,
				t_label *label_list)
{
  char		*label_tmp;
  int		pos;
  extern int	num_line;

  if ((label_tmp = get_label_from_line(label)) == NULL)
    return (0);
  pos = find_label_addr(label_list, label_tmp);
  if (pos < 0)
    {
      my_fprintf(2, "label %s undefine line %d\n", label_tmp, num_line);
      error_msg = TRUE;
      return (0);
    }
  free(label_tmp);
  return (pos - pc_addr);
}

int		my_get_label_length(char *label)
{
  int		i;
  int		j;

  i = 0;
  while (label[i] && (label[i] == ' ' || label[i] == '\t'))
    ++i;
  while (label[i])
    {
      j = 0;
      while (LABEL_CHARS[j] && label[i] != LABEL_CHARS[j])
	++j;
      if (!LABEL_CHARS[j])
	break ;
      ++i;
    }
  return (i);
}

int		my_get_line_addr(char *line, char has_label)
{
  int		num_funct;
  int		res;
  extern op_t	op_tab[];
  char		line_size[4];
  int		i;
  t_parsed_line	parsed_line;

  line = has_label ? line + my_get_label_length(line) + 1 : line;
  parsed_line = get_parsed_line(line, SEPARATOR_CHAR);
  if (parsed_line.fct_name == NULL)
    {
      error_msg = TRUE;
      return (0);
    }
  if ((num_funct = my_get_fct_num(&parsed_line)) < 0)
    return (0);
  res = 1;
  if (op_tab[num_funct].nbr_args > 1 ||
      my_strcmp(parsed_line.fct_name, "aff") == 0)
    ++res;
  decode_codage_byte(&parsed_line, line_size);
  i = 0;
  while (line_size[i] != 0)
    res += line_size[i++];
  return ((i == 0) ? 0 : res);
}
