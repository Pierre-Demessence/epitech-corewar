/*
** instruction0.c for corewar in /home/rouchy_a/Desktop/corewar/git/epitech-corewar/ASM
** 
** Made by rouchy_a
** Login   <rouchy_a@epitech.net>
** 
** Started on  Thu Dec 12 13:11:11 2013 rouchy_a
** Last update Sun Dec 15 22:29:23 2013 a
*/

#include	<stdlib.h>
#include	"my.h"
#include	"op.h"
#include	"asm.h"

extern op_t	op_tab[];
extern int	num_line;
extern char	error_msg;

int		my_type_arg(char *arg, int n_arg)
{
  long		nbr;

  if (arg[0] == 'r' || arg[0] == 'R')
    {
      nbr = my_getnbr(arg + 1);
      if (nbr >= 0 && nbr <= REG_NUMBER)
	return (T_REG);
      my_fprintf(2, "Error : no such register line %d\n", num_line);
    }
  else if (arg[0] == DIRECT_CHAR)
    return (T_DIR);
  else if ((arg[0] >= '0' && arg[0] <= '9') ||
	   arg[0] == '-' || (arg[0] == LABEL_CHAR))
    return (T_IND);
  my_fprintf(2, "incorrect arg nb %d line : %d\n", n_arg + 1, num_line);
  return (0);
}

int		my_check_if_is_index(char *fct_name, int num_arg)
{
  if (my_strcmp(fct_name, "zjmp") == 0)
    return (1);
  else if ((my_strcmp(fct_name, "ldi") == 0 ||
	    my_strcmp(fct_name, "lldi") == 0) &&
	   (num_arg == 0 || num_arg == 1))
    return (1);
  else if (my_strcmp(fct_name, "sti") == 0 && (num_arg == 1 || num_arg == 2))
    return (1);
  else if (my_strcmp(fct_name, "fork") == 0 ||
	   my_strcmp(fct_name, "lfork") == 0)
    return (1);
  else
    return (0);
}

int		my_get_size_arg(char *arg, char *fct_name, int num_arg)
{
  if (arg[0] == 'r' || arg[0] == 'R')
    return (1);
  else if (my_check_if_is_index(fct_name, num_arg) == 1)
    return (IND_SIZE);
  else if (arg[0] == DIRECT_CHAR)
    return (DIR_SIZE);
  else
    return (IND_SIZE);
}

int		my_check_args(t_parsed_line *parsed_line, int *prog_size,
			      int fd, t_label *label_list)
{
  int		c_args;
  int		num_funct;
  int		nb_bytes;
  int		add_to_size;
  int		tab_int[3];

  tab_int[0] = fd;
  tab_int[1] = *prog_size;
  add_to_size = 0;
  num_funct = my_get_fct_num(parsed_line);
  c_args = -1;
  if (my_tablen(parsed_line->tab_args) < op_tab[num_funct].nbr_args)
    return (my_error(1));
  while (++c_args < op_tab[num_funct].nbr_args)
    {
      tab_int[2] = c_args;
      if ((nb_bytes = my_arg(parsed_line, label_list, tab_int)) == -1)
	return (-1);
      add_to_size += nb_bytes;
    }
  *prog_size += add_to_size;
  return (0);
}

int		my_make_instruction_line(char *line, int fd,
					 int *prog_size, t_label *label_list)
{
  t_parsed_line	parsed_line;
  int		num_funct;
  int		add_to_size;
  char		encryption_bit;

  add_to_size = 1;
  parsed_line = get_parsed_line(line, SEPARATOR_CHAR);
  if (parsed_line.fct_name == NULL)
    return (-1);
  num_funct = my_get_fct_num(&parsed_line);
  my_fputchar(fd, num_funct + 1);
  if (op_tab[num_funct].nbr_args > 1 ||
      my_strcmp(parsed_line.fct_name, "aff") == 0)
    {
      encryption_bit = my_get_encryption_bit(parsed_line.tab_args,
					     op_tab[num_funct].nbr_args);
      if (encryption_bit == 0)
	return (my_error(1));
      my_fputchar(fd, encryption_bit);
      add_to_size++;
    }
  if (my_check_args(&parsed_line, prog_size, fd, label_list) == -1)
    return (-1);
  *prog_size += add_to_size;
  return ((error_msg == TRUE) ? -1 : 0);
}
