/*
** decode_func.c for corewar in /home/thing-_a/rendu/git_repo/epitech-corewar/ASM
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Tue Dec  3 19:11:59 2013 a
** Last update Sun Dec 15 14:09:57 2013 a
*/

#include <stdlib.h>
#include "op.h"
#include "asm.h"
#include "my.h"

void		get_real_size(t_parsed_line *line, char res[4])
{
  if (my_strcmp(line->fct_name, "zjmp") == 0 ||
      my_strcmp(line->fct_name, "fork") == 0 ||
      my_strcmp(line->fct_name, "lfork") == 0)
    res[0] = res[0] == 1 ? 1 : IND_SIZE;
  else if (my_strcmp(line->fct_name, "ldi") == 0 ||
	   my_strcmp(line->fct_name, "lldi") == 0)
    {
      res[0] = res[0] == 1 ? 1 : IND_SIZE;
      res[1] = res[1] == 1 ? 1 : IND_SIZE;
    }
  else if (my_strcmp(line->fct_name, "sti") == 0)
    {
      res[1] = res[1] == 1 ? 1 : IND_SIZE;
      res[2] = res[2] == 1 ? 1 : IND_SIZE;
    }
}

void		decode_codage_byte(t_parsed_line *line, char res[4])
{
  int		pos;
  char		*tmp_str;

  pos = 0;
  while (pos < 4)
    res[pos++] = 0;
  pos = 0;
  while (line->tab_args[pos] != NULL)
    {
      tmp_str = my_total_epur_str(line->tab_args[pos]);
      if (tmp_str)
	{
	  if ((tmp_str[0] == 'r') || (tmp_str[0] == 'R'))
	    res[pos] = 1;
	  else if (tmp_str[0] == DIRECT_CHAR)
	    res[pos] = DIR_SIZE;
	  else
	    res[pos] = IND_SIZE;
	  free(tmp_str);
	}
      pos = pos + 1;
    }
  get_real_size(line, res);
}
