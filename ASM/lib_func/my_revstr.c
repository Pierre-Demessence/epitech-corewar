/*
** my_revstr.c for Day 06 in /home/thing-_a/rendu/Piscine-C-Jour_06/ex_03
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Mon Oct  7 18:18:34 2013 a
** Last update Sun Dec 15 18:17:05 2013 a
*/

#include "my.h"

char	*my_revstr(char *str)
{
  char	tmp;
  int	i;
  int	j;

  if (!str)
    return (str);
  i = 0;
  j = my_strlen(str) - 1;
  while (i < j)
    {
      tmp = str[i];
      str[i] = str[j];
      str[j] = tmp;
      i++;
      j--;
    }
  return (str);
}
