/*
** my_strncpy.c for Day 06 in /home/thing-_a/rendu/Piscine-C-Jour_06/ex_02
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Mon Oct  7 18:07:57 2013 a
** Last update Sun Dec 15 18:11:00 2013 a
*/

char	*my_strncpy(char *dest, char *src, int n)
{
  int	i;

  i = 0;
  while (i < n && src[i] != '\0')
    {
      dest[i] = src[i];
      ++i;
    }
  while (i <= n)
    {
      dest[i] = '\0';
      ++i;
    }
  return (dest);
}
