/*
** my_put_nbr.c for Day 03 in /home/thing-_a/rendu/Piscine-C-Jour_03
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Wed Oct  2 10:47:45 2013 a
** Last update Mon Dec  2 16:30:02 2013 a
*/

#include "my.h"

int	is_neg(int fd, int nb)
{
  if (nb >= 0)
    nb = nb * -1;
  else
    my_fputchar(fd, '-');
  return (nb);
}

int	find_coef(int nb)
{
  int	coef;

  coef = 1;
  while (nb / coef <= -10)
    coef = coef * 10;
  return (coef);
}

int	my_put_nbr(int nb)
{
  int	coef;
  int	nb_char;

  nb = is_neg(1, nb);
  coef = find_coef(nb);
  while (coef > 0)
    {
      nb_char = ((nb  / coef) * -1) + '0';
      my_putchar(nb_char);
      nb = nb % coef;
      coef = coef / 10;
    }
  return (0);
}

int	my_fput_nbr(int fd, int nb)
{
  int	coef;
  int	nb_char;

  nb = is_neg(fd, nb);
  coef = find_coef(nb);
  while (coef > 0)
    {
      nb_char = ((nb  / coef) * -1) + '0';
      my_fputchar(fd, nb_char);
      nb = nb % coef;
      coef = coef / 10;
    }
  return (0);
}
