/*
** my_putstr.c for Day 04 in /home/thing-_a/rendu/Piscine-C-Jour_04
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Thu Oct  3 09:03:16 2013 a
** Last update Mon Dec  2 16:36:25 2013 a
*/

#include <unistd.h>
#include "my.h"

int	my_putstr(char *str)
{
  int	i;

  i = 0;
  if (!str)
    return (0);
  while (str[i])
    ++i;
  return (write(1, str, i));
}

int	my_fputstr(int fd, char *str)
{
  int	i;

  i = 0;
  if (!str)
    return (0);
  while (str[i])
    ++i;
  return (write(fd, str, i));
}
