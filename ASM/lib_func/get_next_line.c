/*
** get_next_line.c for get_next_line in /home/taieb_t/rendu/CPE_Get_next_line
** 
** Made by taieb_t$
** Login   <taieb_t@epitech.net>
** 
** Started on  Sat Nov 23 13:16:25 2013 taieb_t$
** Last update Sun Dec 15 18:16:38 2013 a
*/

#include <stdlib.h>
#include <unistd.h>
#include "my.h"
#include "asm.h"

char		*my_strcat_line(char *s1, char *s2)
{
  char		*res;
  int		pos;
  int		free_pos;

  if ((res = my_malloc(my_strlen(s1) + my_strlen(s2) + 1)) == NULL)
    return (NULL);
  pos = 0;
  free_pos = 0;
  while (s1 != NULL && s1[free_pos])
    {
      res[pos++] = s1[free_pos];
      free_pos = free_pos + 1;
    }
  while (s2 != NULL && *s2 && *s2 != '\n')
    {
      res[pos++] = *s2;
      s2 = s2 + 1;
    }
  free(s1);
  res[pos] = 0;
  return (res);
}

int		get_tempo_value(char *buffer, char *tempo, char *bool)
{
  int		pos;
  int		i;

  *bool = 0;
  pos = 0;
  i = -1;
  while (buffer[++i])
    {
      if (*bool)
	tempo[pos++] = buffer[i];
      if (buffer[i] == '\n')
	*bool = 1;
      buffer[i] = 0;
    }
  if (!*bool)
    {
      i = -1;
      while (++i <= (BUFF_SIZE + 1));
      tempo[i] = 0;
    }
  return (*bool);
}

char		*search_next_line(const int fd, char *str, int res)
{
  char		buffer[BUFF_SIZE];
  static char	tempo[BUFF_SIZE];
  static char	bool = 0;

  if (bool)
    {
      str = my_strcat_line(str, tempo);
      if (get_tempo_value(tempo, tempo, &bool))
	return (str);
    }
  while (res != 0)
    {
      if ((res = read(fd, buffer, BUFF_SIZE)) == -1)
	return (NULL);
      buffer[res] = 0;
      if ((str = my_strcat_line(str, buffer)) == NULL)
	return (NULL);
      if (res == 0 && my_strlen(str) > 0)
	return (str);
      if (get_tempo_value(buffer, tempo, &bool))
	return (str);
    }
  free(str);
  return (NULL);
}

char		*get_next_line(const int fd)
{
  char		*str;
  int		res;

  res = 1;
  str = NULL;
  return (search_next_line(fd, str, res));
}
