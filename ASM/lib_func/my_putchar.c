/*
** my_putchar.c for Lib in /home/thing-_a/rendu/Piscine-C-lib/my
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Tue Oct  8 18:40:02 2013 a
** Last update Mon Dec  2 16:31:02 2013 a
*/

#include <unistd.h>
#include "my.h"

void	my_putchar(char c)
{
  (void)write(1, &c, 1);
}

void	my_fputchar(int fd, char c)
{
  (void)write(fd, &c, 1);
}
