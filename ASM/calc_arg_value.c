/*
** calc_arg_value.c for corewar in /home/rouchy_a/Desktop/corewar/epitech-corewar/epitech-corewar/ASM
** 
** Made by rouchy_a
** Login   <rouchy_a@epitech.net>
** 
** Started on  Sun Dec 15 16:59:40 2013 rouchy_a
** Last update Sun Dec 15 23:12:02 2013 a
*/

#include	<stdlib.h>
#include	"my.h"
#include	"op.h"
#include	"asm.h"

extern int	num_line;
extern op_t	op_tab[];
extern char	error_msg;

int	get_value_el(char *arg, int prog_size, t_label *label_list)
{
  if (arg[0] == LABEL_CHAR)
    return (my_label_encode(prog_size, arg + 1, label_list));
  else
    return (my_getnbr(arg));
}

int	my_make_calc(char *el, int prog_size, t_label *label_list)
{
  char	op;

  op = el[0];
  el++;
  if (op == '+')
    return (get_value_el(el, prog_size, label_list));
  else
    return (-1 * get_value_el(el, prog_size, label_list));
}

int	my_get_int_to_write(char *arg, int prog_size, t_label *label_list)
{
  int	cpt;
  int	result;

  result = 0;
  cpt = my_strlen(arg);
  while (cpt > 0)
    {
      if (arg[cpt] == '+' || arg[cpt] == '-')
	result += my_make_calc(arg + cpt, prog_size, label_list);
      if (error_msg == TRUE)
	return (-1);
      cpt--;
    }
  result += get_value_el(arg, prog_size, label_list);
  return (result);
}

/*
** tab_int[0] = fd;
** tab_int[1] = prog_size
** tab_int[2] = c_args
*/
int	my_arg(t_parsed_line *parsed_line,
	       t_label *label_list, int tab_int[3])
{
  char	*arg;
  int	t_arg;
  int	nb_bytes;
  int	num_funct;
  int	result;

  num_funct = my_get_fct_num(parsed_line);
  if ((arg = my_total_epur_str((*parsed_line).tab_args[tab_int[2]])) == NULL)
    return (-1);
  if (((t_arg = my_type_arg(arg, tab_int[2])) &
       op_tab[num_funct].type[tab_int[2]]) == 0)
    return (-1);
  nb_bytes = my_get_size_arg(arg, op_tab[num_funct].mnemonique, tab_int[2]);
  result = my_get_int_to_write((t_arg == 4) ? arg : (arg + 1),
				     tab_int[1], label_list);
  if (error_msg == TRUE)
    return (-1);
  if (t_arg == T_IND && ((tab_int[1] + result < 0) || (result > IDX_MOD)))
    my_fprintf(2, "Warning Indirection to far line %d\n", num_line);
  else if (t_arg == T_DIR && (tab_int[1] + result < 0 || result > MEM_SIZE))
    my_fprintf(2, "Warning Direct to big line %d\n", num_line);
  my_write_nbr_bites(result, nb_bytes, tab_int[0]);
  free(arg);
  return (nb_bytes);
}
