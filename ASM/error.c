/*
** error.c for corewar in /home/rouchy_a/Desktop/corewar/epitech-corewar/epitech-corewar/ASM
** 
** Made by rouchy_a
** Login   <rouchy_a@epitech.net>
** 
** Started on  Sun Dec 15 11:03:58 2013 rouchy_a
** Last update Sun Dec 15 22:04:44 2013 rouchy_a
*/

#include	<stdlib.h>
#include	"my.h"
#include	"op.h"
#include	"asm.h"

extern op_t	op_tab[];
extern int	num_line;

int		my_error(int num_error)
{
  if (num_error == 1)
    my_fprintf(2, "Not enough arguments line %d\n", num_line);
  else
    my_fprintf(2, "error line  %d\n", num_line);
  if (num_error == 1)
    return (-1);
  else
    return (0);
}
