/*
** tools_asm.c for corewar in /home/rouchy_a/Desktop/corewar/epitech-corewar/ASM
** 
** Made by rouchy_a
** Login   <rouchy_a@epitech.net>
** 
** Started on  Tue Dec 10 12:55:51 2013 rouchy_a
** Last update Sun Dec 15 22:13:21 2013 a
*/

#include	<stdlib.h>
#include	<unistd.h>
#include	"my.h"
#include	"asm.h"
#include	"op.h"

char		*my_total_epur_str(char *str)
{
  char		*result;
  int		cpt;
  int		size;

  size = 0;
  cpt = -1;
  while (str[++cpt] != 0)
    if ((str[cpt] != '\t') && (str[cpt] != ' '))
      size++;
  if ((result = my_malloc(sizeof(char) * (size + 1))) == NULL)
    return (NULL);
  cpt = -1;
  size = -1;
  while (str[++cpt] != 0)
    if ((str[cpt] != '\t') && (str[cpt] != ' '))
      result[++size] = str[cpt];
  result[++size] = 0;
  return (result);
}

char		my_get_encryption_bit(char **tab_args, int nb_args)
{
  char		result;
  char		*tmp_str;
  int		cpt;
  int		num_elem;

  result = 0;
  cpt = 6;
  num_elem = -1;
  while (++num_elem < nb_args && tab_args[num_elem] != NULL)
    {
      tmp_str = my_total_epur_str(tab_args[num_elem]);
      if ((tmp_str[0] == 'r') || (tmp_str[0] == 'R'))
	result += 1 << cpt;
      else if (tmp_str[0] == DIRECT_CHAR)
	result += 2 << cpt;
      else
	result += 3 << cpt;
      cpt -= 2;
      free(tmp_str);
    }
  if (num_elem < nb_args)
    return (0);
  return (result);
}

t_parsed_line	get_parsed_line(char *line, char separator)
{
  t_parsed_line	result;
  int		cpt_line;
  int		cpt_fct_name;

  cpt_line = 0;
  cpt_fct_name = -1;
  result.fct_name = my_malloc(sizeof(char) * my_strlen(line));
  if (result.fct_name == NULL)
    return (result);
  while (((line[cpt_line] == '\t') || (line[cpt_line] == ' ')) &&
	 (line[cpt_line] != '\0'))
    cpt_line++;
  while (((line[cpt_line] != '\t') && (line[cpt_line] != ' ')) &&
	 (line[cpt_line] != '\0'))
    result.fct_name[++cpt_fct_name] = line[cpt_line++];
  result.fct_name[++cpt_fct_name] = 0;
  result.tab_args = my_str_to_wordtab(line + cpt_line, &separator);
  return (result);
}

void		my_write_nbr_bites(int	nb, int nb_bits, int fd)
{
  nb_bits--;
  while (nb_bits >= 0)
    my_fputchar(fd, 0xff & (nb >> (nb_bits-- * 8)));
}

int		my_tablen(char **tab)
{
  int		cpt;

  if (tab == NULL)
    return (0);
  cpt = 0;
  while (tab[cpt] != NULL)
    ++cpt;
  return (cpt);
}
