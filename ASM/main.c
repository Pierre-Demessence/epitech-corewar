/*
** mainc.c for corewar in /home/rouchy_a/Desktop/corewar/v1
** 
** Made by rouchy_a
** Login   <rouchy_a@epitech.net>
** 
** Started on  Wed Nov 27 11:07:12 2013 rouchy_a
** Last update Sun Dec 15 17:44:57 2013 a
*/

#include <stdlib.h>
#include <unistd.h>
#include "my.h"
#include "asm.h"
#include "op.h"

int		num_line = 0;
char		error_msg = FALSE;

int		my_make_file(int tab_fd[2], int *prog_size)
{
  char		*line;
  t_label	*label_list;

  label_list = get_label_list(tab_fd[0]);
  if (error_msg == TRUE)
    return (-1);
  num_line = 0;
  if (my_write_header(tab_fd) == -1)
    return (-1);
  while ((line = get_next_line(tab_fd[0])) != NULL)
    {
      num_line++;
      if (line[0] != '\0')
	if (my_make_line(line, tab_fd[1], prog_size, label_list) == -1)
	  return (-1);
      free(line);
    }
  return (0);
}

char		*new_file_name(char *old_name)
{
  char		*result;
  int		i;

  result = my_malloc(sizeof(char) * (my_strlen(old_name) + 3));
  if (result == NULL)
    return (NULL);
  i = -1;
  while (old_name[++i] && ((old_name[i] == '.' && old_name[i + 1] != 's')
			      || old_name[i] != '.'))
    result[i] = old_name[i];
  result[i] = 0;
  result = my_strcat(result, ".cor");
  return (result);
}

int		do_arg(char *arg)
{
  int		fd_src;
  int		fd_result;
  char		*file_name;
  int		prog_size;
  int		tab_fd[2];

  prog_size = 0;
  if (((fd_src = my_open_src(arg)) == -1) ||
      ((file_name = new_file_name(arg)) == NULL) ||
      ((fd_result = my_open_dest(file_name)) == -1))
    return (-1);
  tab_fd[0] = fd_src;
  tab_fd[1] = fd_result;
  if (my_make_file(tab_fd, &prog_size) == -1 ||
      (lseek(tab_fd[1], (4 + PROG_NAME_LENGTH + 4), SEEK_SET) == -1))
    return (-1);
  my_write_nbr_bites(prog_size, 4, tab_fd[1]);
  my_fprintf(1, "Assembling %s\n", arg);
  my_display_datas_header(tab_fd[1]);
  free(file_name);
  (void)close(fd_src);
  (void)close(fd_result);
  return (0);
}

int		main(int argc, char **argv)
{
  int		cpt;

  if (argc > 1)
    {
      cpt = 0;
      while (++cpt < argc)
	if (do_arg(argv[cpt]) == -1)
	  return (-1);
      return (0);
    }
  else
    my_fprintf(1, "Usage :\n./AsmX file_name[.s] ...\n");
  return (-1);
}
