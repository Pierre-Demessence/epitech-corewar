/*
** mainc.c for corewar in /home/rouchy_a/Desktop/corewar/v1
** 
** Made by rouchy_a
** Login   <rouchy_a@epitech.net>
** 
** Started on  Wed Nov 27 11:07:12 2013 rouchy_a
** Last update Sun Dec 15 19:01:16 2013 a
*/

#include	<stdlib.h>
#include	"my.h"
#include	"op.h"
#include	"asm.h"

int		my_get_fct_num(t_parsed_line *parsed_line)
{
  int		cpt_op_tab;
  extern op_t   op_tab[];

  cpt_op_tab = -1;
  while (op_tab[++cpt_op_tab].mnemonique != 0)
    if (my_strcmp(op_tab[cpt_op_tab].mnemonique, (*parsed_line).fct_name) == 0)
      return (cpt_op_tab);
  return (-1);
}

int		check_if_is_instruction(char *line)
{
  int		cpt;
  extern op_t	op_tab[];

  cpt = -1;
  while (op_tab[++cpt].mnemonique != 0)
    if (my_strncmp(line, op_tab[cpt].mnemonique,
		   my_strlen(op_tab[cpt].mnemonique)) == 0)
      return (1);
  return (0);
}

int			my_make_line(char *l, int fd,
				     int *prg_size, t_label *lab_list)
{
  int			cpt;

  cpt = 0;
  while ((l[cpt] == ' ') || (l[cpt] == '\t'))
    cpt++;
  if (check_if_is_label(l + cpt) == TRUE)
    while (l[cpt] != LABEL_CHAR)
      cpt++;
  if (check_if_is_label(l + cpt) == TRUE)
    while ((l[++cpt] == ' ') || (l[cpt] == '\t'))
      ;
  if ((l[cpt] != COMMENT_CHAR) && (l[cpt] != 0))
    {
      if (my_strncmp(l + cpt, NAME_CMD_STRING, my_strlen(NAME_CMD_STRING)) == 0)
	return (my_write_cmd(fd, l + cpt, NAME_CMD_STRING) == -1 ? -1 : 0);
      else if (my_strncmp(l + cpt, COMMENT_CMD_STRING,
			  my_strlen(COMMENT_CMD_STRING)) == 0)
	return (my_write_cmd(fd, l + cpt, COMMENT_CMD_STRING) == -1 ? -1 : 0);
      else if (l[cpt] != '.' &&
	       ((check_if_is_instruction(l + cpt) == FALSE) ||
		my_make_instruction_line(l + cpt, fd, prg_size, lab_list) == -1))
	return (-1);
    }
  return (0);
}
