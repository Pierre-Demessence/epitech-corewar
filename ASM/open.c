/*
** open.c for corewar in /home/rouchy_a/Desktop/corewar/v1
** 
** Made by rouchy_a
** Login   <rouchy_a@epitech.net>
** 
** Started on  Mon Dec  2 14:27:24 2013 rouchy_a
** Last update Sun Dec 15 17:43:22 2013 a
*/

#include	<stdlib.h>
#include	<sys/stat.h>
#include	<fcntl.h>
#include	<unistd.h>
#include	"my.h"
#include	"asm.h"

/* ouverture du fichier passé en paramètre dans le main */
int	my_open_src(char *name)
{
  int	fd;
  char	*file_name;
  int	i;

  file_name = my_malloc(sizeof(char) * (my_strlen(name) + 2));
  if (file_name == NULL)
    return (-1);
  i = -1;
  while (name[++i] != '\0' && ((name[i] == '.' && name[i + 1] != 's')
			       || name[i] != '.'))
    file_name[i] = name[i];
  file_name[i] = '\0';
  file_name = my_strcat(file_name, ".s");
  fd = open(file_name, O_RDONLY);
  free(file_name);
  if (fd == -1)
    {
      (void)my_fprintf(2, "File %s not accessible\n", name);
      return (-1);
    }
  return (fd);
}

/* ouverture / création du fichier ".cor" */
int	my_open_dest(char *name)
{
  int	fd;

  fd = open(name, O_CREAT | O_RDWR | O_TRUNC,
	    S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
  if (fd == -1)
    {
      (void)my_fprintf(2, "File %s not accessible\n", name);
      return (-1);
    }
  return (fd);
}
