/*
** malloc.c for corewar in /home/rouchy_a/Desktop/corewar/v1
** 
** Made by rouchy_a
** Login   <rouchy_a@epitech.net>
** 
** Started on  Mon Dec  2 16:13:12 2013 rouchy_a
** Last update Mon Dec  2 17:47:45 2013 a
*/

#include	<stdlib.h>
#include	<unistd.h>
#include	"my.h"
#include	"asm.h"

/* verification du malloc */
void	*my_malloc(int len)
{
  void	*result;

  result = malloc(len);
  if (result == NULL)
    my_putstr_error("Can't perform malloc\n");
  return (result);
}
