/*
** write_fd.c for corewar in /home/rouchy_a/Desktop/corewar/epitech-corewar/ASM
** 
** Made by rouchy_a
** Login   <rouchy_a@epitech.net>
** 
** Started on  Tue Dec  3 18:53:53 2013 rouchy_a
** Last update Sun Dec 15 16:51:20 2013 rouchy_a
*/

#include	<unistd.h>
#include	<stdlib.h>
#include	"my.h"
#include	"asm.h"
#include	"op.h"

extern int	num_line;

int		my_write_datas(int fd, char *line, int result_size, char *result)
{
  int		size_data;
  int		cpt_header;

  size_data = 0;
  cpt_header = -1;
  while ((line[size_data] != '\0') && (line[size_data] != '"'))
    size_data++;
  while ((line[++size_data] != '\0') && (line[size_data] != '"') &&
	 (++cpt_header < result_size))
    result[cpt_header] = line[size_data];
  if (line[size_data] != '"')
    {
      my_fprintf(2, "Syntax error line %d\n", num_line);
      return (-1);
    }
  result[++cpt_header] = '\0';
  my_fputstr(fd, result);
  while (++cpt_header <= result_size)
    my_fputchar(fd, 0);
  return (0);
}

int		my_write_cmd(int fd, char *line, char *element)
{
  char		*result;
  int		result_size;
  int		size_begin;
  header_t	header;

  if (my_strcmp(element, NAME_CMD_STRING) == 0)
    {
      result = header.prog_name;
      result_size = PROG_NAME_LENGTH;
      size_begin = 4;
    }
  else
    {
      result = header.comment;
      result_size = COMMENT_LENGTH;
      size_begin = 4 + PROG_NAME_LENGTH + 8;
    }
  if (lseek(fd, size_begin, SEEK_SET) == -1)
    return (-1);
  if ((my_write_datas(fd, line, result_size, result) == -1))
    return (-1);
  if (lseek(fd, 0, SEEK_END) == -1)
    return (-1);
  return (0);
}

void		my_write_magic(int tab_fd[2])
{
  char		*magic;
  int		magic_number;
  int		cpt;

  magic_number = COREWAR_EXEC_MAGIC;
  magic = (char*)(&(magic_number));
  cpt = my_strlen(magic);
  while (cpt++ < 4)
    my_fputchar(tab_fd[1], 0);
  my_fprintf(tab_fd[1], "%s", my_revstr(magic));
}

int		my_write_header(int tab_fd[2])
{
  int	cpt;

  my_write_magic(tab_fd);
  cpt = -1;
  while (++cpt < PROG_NAME_LENGTH + COMMENT_LENGTH + 12)
    my_fputchar(tab_fd[1], 0);
  return (0);
}

int		my_display_datas_header(int fd)
{
  int	len;
  char *buff_name[PROG_NAME_LENGTH + 1];
  char *buff_comment[COMMENT_LENGTH + 1];

  if (lseek(fd, 4, SEEK_SET) == -1)
    return (-1);
  if ((len = read(fd, buff_name, PROG_NAME_LENGTH)) == -1)
    return (-1);
  my_fprintf(1, "\t%s\n", buff_name);
  if (lseek(fd, 4 + PROG_NAME_LENGTH + 8, SEEK_SET) == -1)
    return (-1);
  if ((len = read(fd, buff_comment, COMMENT_LENGTH)) == -1)
    return (-1);
  my_fprintf(1, "\t%s\n", buff_comment);
  return (0);
}
