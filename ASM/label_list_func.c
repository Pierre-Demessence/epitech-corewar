/*
** label_list_func.c for corewar in /home/thing-_a/rendu/git_repo/epitech-corewar/ASM
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Mon Dec  2 16:07:57 2013 a
** Last update Sun Dec 15 14:59:04 2013 a
*/

#include <stdlib.h>
#include <unistd.h>
#include "op.h"
#include "asm.h"
#include "my.h"

extern char	error_msg;

t_label		*free_label_list(t_label *label_list)
{
  t_label	*tmp;

  while (label_list)
    {
      tmp = label_list;
      free(label_list->label);
      label_list = label_list->next;
      free(tmp);
    }
  return (NULL);
}

int		find_label_addr(t_label *label_list, char *label)
{
  while (label_list)
    {
      if (my_strcmp(label_list->label, label) == 0)
	return (label_list->addr);
      label_list = label_list->next;
    }
  return (-1);
}

t_label		*add_new_label(char *label, int addr, t_label **label_list,
			       int line_number)
{
  t_label	*new_elem;
  char		*label_tmp;

  label_tmp = get_label_from_line(label);
  if (!label_tmp)
    return (free_label_list(*label_list));
  if (find_label_addr(*label_list, label_tmp) >= 0)
    {
      my_fprintf(2, "Syntax error on line %d\n", line_number);
      error_msg = TRUE;
      return (free_label_list(*label_list));
    }
  if ((new_elem = my_malloc(sizeof(t_label))) == NULL)
    {
      error_msg = TRUE;
      return (free_label_list(*label_list));
    }
  new_elem->label = label_tmp;
  new_elem->addr = addr;
  new_elem->next = *label_list;
  if (!*label_list)
    *label_list = new_elem;
  return (new_elem);
}

int		check_if_is_label(char *instruction)
{
  int		i;
  int		j;

  i = 0;
  while (instruction[i] && (instruction[i] == ' ' || instruction[i] == '\t'))
    ++i;
  while (instruction[i])
    {
      j = 0;
      while (LABEL_CHARS[j] && instruction[i] != LABEL_CHARS[j])
	++j;
      if (!LABEL_CHARS[j])
	break ;
      ++i;
    }
  return (instruction[i] == LABEL_CHAR);
}

t_label		*get_label_list(int fd_src)
{
  t_label	*label_list;
  char		*line;
  int		addr;
  int		line_num;
  char		has_label;

  addr = 0;
  line_num = 0;
  label_list = NULL;
  while ((line = get_next_line(fd_src)) != NULL)
    {
      if ((has_label = check_if_is_label(line)) == TRUE)
	{
	  if (!(label_list = add_new_label(line, addr, &label_list, line_num)))
	    return (NULL);
	}
      addr += my_get_line_addr(line, has_label);
      ++line_num;
    }
  if (lseek(fd_src, 0, SEEK_SET) == -1)
    {
      error_msg = TRUE;
      return (NULL);
    }
  return (label_list);
}
