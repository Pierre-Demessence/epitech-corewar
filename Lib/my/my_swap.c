/*
** my_swap.c for *toto in /home/thing-_a/rendu/Piscine-C-Jour_04
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Thu Oct  3 17:38:41 2013 a
** Last update Fri Oct 11 07:24:30 2013 a
*/

int	my_swap(int *a, int *b)
{
  int	tmp;

  tmp = *a;
  *a = *b;
  *b = tmp;
  return (0);
}
