/*
** my.h for Day 09 in /home/thing-_a/rendu/Piscine-C-include
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Fri Oct 11 07:18:14 2013 a
** Last update Sun Dec 15 11:59:25 2013 rouchy_a
*/

#ifndef		MY_H_
# define	MY_H_

# define TRUE	1
# define FALSE	0
# define BUFF_SIZE 42

/*
** Show funtions
*/
void		my_putchar(char c);
void		my_fputchar(int fd, char c);
int		my_put_nbr(int nb);
int		my_fput_nbr(int fd, int nb);
int		my_putstr(char *str);
int		my_fputstr(int fd, char *str);
int		my_showmem(char *str, int size);
int		my_showstr(char *str);
int		my_show_wordtab(char **);
int		my_printf(char *str, ...);
int		my_fprintf(int fd, char *str, ...);
int		my_putnbr_base(int nbr, char *base);
void		my_putstr_error(char *str);

/*
** String functions
*/
char		*my_revstr(char *str);
char		*my_strcapitalize(char *str);
char		*my_strcat(char *dest, char *str);
int		my_strcmp(char *s1, char *s2);
char		*my_strcpy(char *dest, char *src);
int		my_str_isalpha(char *str);
int		my_str_islower(char *str);
int		my_str_isnum(char *str);
int		my_str_isprintable(char *str);
int		my_strlen(char *str);
char		*my_strlowcase(char *str);
char		*my_strncat(char *dest, char *str, int nb);
int		my_strncmp(char *s1, char *s2, int n);
char		*my_strncpy(char *dest, char *src, int n);
char		*my_strstr(char *str, char *to_find);
char		*my_strupcase(char *str);
char		*my_strdup(char *src);
char		**my_str_to_wordtab(char *str, char *cut);
int		my_swap_char(char **s1, char **s2);
char		*get_next_line(const int fd);

/*
** Number functions
*/
long		my_getnbr(char *str);
void		my_sort_int_tab(int *, int );
int		my_swap(int *a, int *b);

#endif		/* !MY_H_ */

