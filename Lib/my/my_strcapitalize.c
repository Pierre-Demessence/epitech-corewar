/*
** my_strcapitalize.c for Day 06 in /home/thing-_a/rendu/Piscine-C-Jour_06/ex_09
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Mon Oct  7 20:02:30 2013 a
** Last update Tue Oct 22 20:19:54 2013 a
*/

int	is_alpha_num(char c, char is_num)
{
  if ((c >= 'a' && c <= 'z') ||  (c >= '0' && c <= '9' && is_num == 1))
    return (1);
  if (c >= 'A' && c <= 'Z' && is_num == 1)
    return (1);
  return (0);
}

char	*my_strcapitalize(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if (i == 0 && is_alpha_num(str[i], 0))
	str[i] = str[i] - ('a' - 'A');
      else
	{
	  if (i > 0 && !is_alpha_num(str[i - 1], 1) && is_alpha_num(str[i], 0))
	    str[i] = str[i] - ('a' - 'A');
	}
      i++;
    }
  return (str);
}
