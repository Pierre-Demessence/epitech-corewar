/*
** my_str_to_wordtab.c for Day 08 in /home/thing-_a/rendu/Piscine-C-Jour_08/ex_04
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Wed Oct  9 20:13:00 2013 a
** Last update Mon Dec  2 16:39:59 2013 a
*/

#include <stdlib.h>
#include "my.h"

int	my_char_isalphanum(char c, char *cut)
{
  int	i;

  i = 0;
  if (!cut)
    return (0);
  while (cut[i])
    {
      if (c == cut[i])
	return (1);
      ++i;
    }
  return (0);
}

int	find_tab_len(char *str, char *cut)
{
  int	i;
  int	nb_word;

  i = 0;
  nb_word = 0;
  if (!str)
    return (0);
  while (str[i])
    {
      if (my_char_isalphanum(str[i], cut))
	{
	  while (my_char_isalphanum(str[i], cut) && str[i])
	    i = i + 1;
	}
      else
	{
	  nb_word = nb_word + 1;
	  while (!my_char_isalphanum(str[i], cut) && str[i])
	    i = i + 1;
	}
    }
  return (nb_word);
}

char	*get_next_tab(char **tab, int tab_i, char *str, char *cut)
{
  int	i;

  i = 0;
  while (my_char_isalphanum(str[i], cut) == 0 && str[i])
    i = i + 1;
  tab[tab_i] = malloc(i + 1);
  if (tab[tab_i] == NULL)
    return (NULL);
  tab[tab_i] = my_strncpy(tab[tab_i], str, i);
  tab[tab_i][i] = '\0';
  str = str + i;
  return (str);
}

char	*advance_str(char *str, char *cut)
{
  while (*str && my_char_isalphanum(*str, cut) != 0)
    str = str + 1;
  return (str);
}

char	**my_str_to_wordtab(char *str, char *cut)
{
  char	**tab;
  int	tab_i;
  int	tab_len;

  tab_len = find_tab_len(str, cut);
  tab = malloc((tab_len + 1) * sizeof(char *));
  if (tab == NULL)
    return (NULL);
  tab_i = 0;
  while (*str)
    {
      if (my_char_isalphanum(*str, cut) != 0)
	str = advance_str(str, cut);
      else
	{
	  str = get_next_tab(tab, tab_i, str, cut);
	  if (str == NULL)
	    return (NULL);
	  tab_i = tab_i + 1;
	}
    }
  tab[tab_i] = NULL;
  return (tab);
}
