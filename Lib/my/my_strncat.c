/*
** my_strncat.c for Day 07 in /home/thing-_a/rendu/Piscine-C-Jour_07/ex_01
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Tue Oct  8 19:02:03 2013 a
** Last update Mon Oct 21 15:21:15 2013 a
*/

#include "my.h"

char	*my_strncat(char *dest, char *str, int nb)
{
  int	i;
  int	j;

  i = 0;
  j = my_strlen(dest);
  while (str[i] != '\0' && i < nb)
    {
      dest[j] = str[i];
      i++;
      j++;
    }
  dest[j] = '\0';
  return (dest);
}
