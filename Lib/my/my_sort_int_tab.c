/*
** my_sort_int_tab.c for Day 04 in /home/thing-_a/rendu/Piscine-C-Jour_04
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Thu Oct  3 13:37:06 2013 a
** Last update Mon Oct 21 15:19:30 2013 a
*/

#include "my.h"

void	my_sort_int_tab(int *tab, int size)
{
  int	i;
  int	j;

  i = 0;
  while (i < size)
    {
      j = 0;
      while (j < size)
	{
	  if (tab[i] < tab[j])
	    my_swap(&tab[i], &tab[j]);
	  j = j + 1;
	}
      i = i + 1;
    }
}
