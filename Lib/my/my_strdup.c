/*
** my_strdup.c for Day 08 in /home/thing-_a/rendu/Piscine-C-Jour_08/ex_01
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Wed Oct  9 18:56:15 2013 a
** Last update Mon Dec  2 16:35:59 2013 a
*/

#include <stdlib.h>
#include "my.h"

char	*my_strdup(char *src)
{
  int	len;
  char	*cpy;
  int	i;

  if (!src)
    return (NULL);
  len = my_strlen(src);
  cpy = malloc(len + 1);
  if (cpy == NULL)
    return (NULL);
  i = 0;
  while (src[i])
    {
      cpy[i] = src[i];
      i = i + 1;
    }
  cpy[i] = '\0';
  return (cpy);
}
