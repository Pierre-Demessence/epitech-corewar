/*
** my_strlen.c for Day 04 in /home/thing-_a/rendu/Piscine-C-Jour_04
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Thu Oct  3 09:05:22 2013 a
** Last update Mon Dec  2 16:36:56 2013 a
*/

int	my_strlen(char *str)
{
  int	len;

  len = 0;
  if (!str)
    return (0);
  while (str[len] != '\0')
    len = len + 1;
  return (len);
}
