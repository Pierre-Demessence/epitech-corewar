/*
** my_showstr.c for Day 06 in /home/thing-_a/rendu/Piscine-C-Jour_06/ex_17
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Mon Oct 14 11:59:13 2013 a
** Last update Mon Oct 14 14:29:05 2013 a
*/

#include "my.h"

char	is_printable(char c)
{
  if (c < ' ' || c > '~')
    return (FALSE);
  return (TRUE);
}

int	my_showstr(char *str)
{
  int	i;

  i = 0;
  while (str[i])
    {
      if (!is_printable(str[i]))
	{
	  my_putchar('\\');
	  if (str[i] < 16)
	    my_putchar('0');
	  my_putnbr_base(str[i], "0123456789abcdef");
	}
      else
	my_putchar(str[i]);
      i = i + 1;
    }
  return (0);
}
