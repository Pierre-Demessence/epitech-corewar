/*
** my_showmem.c for Day 06 in /home/thing-_a/rendu/Piscine-C-Jour_06/ex_18
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Thu Oct 17 15:53:52 2013 a
** Last update Mon Oct 21 15:18:33 2013 a
*/

#include "my.h"

char	is_printable(char c)
{
  if (c < ' ' || c > '~')
    return (FALSE);
  return (TRUE);
}

void	my_show_addr(char *str)
{
  my_putnbr_base((long int)str, "0123456789abcdef");
  my_putstr(": ");
}

void	my_show_hexa(char *str)
{
  int	i;

  i = 0;
  while (i < 16)
    {
      if (str[i] < 16)
	my_putchar('0');
      my_putnbr_base((int)str[i], "0123456789abcdef");
      if (str[i + 1] < 16)
	my_putchar('0');
      my_putnbr_base((int)str[i + 1], "0123456789abcdef");
      my_putchar(' ');
      i = i + 2;
    }
}

void	my_showline_mem(char *str)
{
  int	i;

  i = 0;
  my_show_addr(str);
  my_show_hexa(str);
  while (i < 16)
    {
      if (is_printable(str[i]))
	my_putchar(str[i]);
      else
	my_putchar('.');
      i = i + 1;
    }
}

int	my_showmem(char *str, int size)
{
  int	i;

  i = 0;
  while (i < size)
    {
      my_showline_mem(str);
      my_putchar('\n');
      str = str + 16;
      i = i + 16;
    }
  return (0);
}
