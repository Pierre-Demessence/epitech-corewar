/*
** my_str_isalpha.c for Day 06 in /home/thing-_a/rendu/Piscine-C-Jour_06/ex_10
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Mon Oct  7 20:11:35 2013 a
** Last update Mon Oct 21 20:32:31 2013 a
*/

int	my_str_isalpha(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if ((str[i] < 'a' || str[i] > 'z') && (str[i] < 'A' || str[i] > 'Z'))
	return (0);
      i++;
    }
  return (1);
}
