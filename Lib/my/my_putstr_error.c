/*
** my_putstr_error.c for Lib in /home/thing-_a/rendu/Piscine-C-lib/my
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Wed Oct 23 14:42:09 2013 a
** Last update Mon Dec  2 16:40:36 2013 a
*/

#include <unistd.h>

void	my_putstr_error(char *str)
{
  int	i;

  i = 0;
  if (str)
    {
      while (str[i])
	i = i + 1;
      (void)write(2, str, i);
    }
}
