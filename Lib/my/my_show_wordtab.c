/*
** my_show_wordtab.c for Day 08 in /home/thing-_a/rendu/Piscine-C-Jour_08/ex_05
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Wed Oct  9 20:39:33 2013 a
** Last update Mon Oct 21 15:19:13 2013 a
*/

#include "my.h"

int	my_show_wordtab(char **tab)
{
  int	i;

  i = 0;
  while (tab[i] != 0)
    {
      my_putstr(tab[i]);
      my_putchar('\n');
      i = i + 1;
    }
  return (0);
}
