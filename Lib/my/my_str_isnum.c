/*
** my_str_isnum.c for Day 06 in /home/thing-_a/rendu/Piscine-C-Jour_06/ex_11
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Mon Oct  7 20:16:17 2013 a
** Last update Fri Oct 11 07:22:35 2013 a
*/

int	my_str_isnum(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if (str[i] < '0' || str[i] > '9')
	return (0);
      i++;
    }
  return (1);
}
