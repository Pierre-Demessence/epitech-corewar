/*
** my_str_isprintable.c for Day 06 in /home/thing-_a/rendu/Piscine-C-Jour_06/ex_11
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Mon Oct  7 20:20:28 2013 a
** Last update Fri Oct 11 07:22:47 2013 a
*/

int	my_str_isprintable(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if (str[i] < 32 || str[i] > 126)
	return (0);
      i++;
    }
  return (1);
}
