/*
** my_strlowercase.c for Day 06 in /home/thing-_a/rendu/Piscine-C-Jour_06/ex_08
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Mon Oct  7 20:01:01 2013 a
** Last update Sun Nov  3 15:51:56 2013 a
*/

char	*my_strlowcase(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if (str[i] >= 'A' && str[i] <= 'Z')
	str[i] = str[i] + ('a' - 'A');
      i++;
    }
  return (str);
}
