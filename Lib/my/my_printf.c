/*
** my_printf.c for my_printf in /home/thing-_a/rendu/Piscine-C-match-nmatch
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Sun Oct 13 11:00:19 2013 a
** Last update Mon Dec  2 16:31:57 2013 a
*/

#include <stdarg.h>
#include "my.h"

int		my_printf(char *str, ...)
{
  va_list	ap;

  va_start(ap, str);
  while (*str)
    {
      if (*str == '%')
	{
	  str = str + 1;
	  if (*str == 's')
	    my_putstr(va_arg(ap, char *));
	  else if (*str  == 'd')
	    my_put_nbr(va_arg(ap, int));
	  else if (*str  == 'c')
	    my_putchar((char)va_arg(ap, int));
	}
      else
	my_putchar(*str);
      str = str + 1;
    }
  va_end(ap);
  return (0);
}

int		my_fprintf(int fd, char *str, ...)
{
  va_list	ap;

  va_start(ap, str);
  while (*str)
    {
      if (*str == '%')
	{
	  str = str + 1;
	  if (*str == 's')
	    my_fputstr(fd, va_arg(ap, char *));
	  else if (*str  == 'd')
	    my_fput_nbr(fd, va_arg(ap, int));
	  else if (*str  == 'c')
	    my_fputchar(fd, (char)va_arg(ap, int));
	}
      else
	my_fputchar(fd, *str);
      str = str + 1;
    }
  va_end(ap);
  return (0);
}
