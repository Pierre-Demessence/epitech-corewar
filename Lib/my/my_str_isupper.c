/*
** my_str_isupper.c for Day 06 in /home/thing-_a/rendu/Piscine-C-Jour_06/ex_11
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Mon Oct  7 20:19:31 2013 a
** Last update Mon Oct  7 20:22:24 2013 a
*/

int	my_str_isupper(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if (str[i] < 'A' || str[i] > 'Z')
	return (0);
      i++;
    }
  return (1);
}
