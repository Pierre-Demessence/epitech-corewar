/*
** my_strstr.c for Day 06 in /home/thing-_a/rendu/Piscine-C-Jour_06/ex_04
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Mon Oct  7 18:21:49 2013 a
** Last update Mon Oct 21 19:32:09 2013 a
*/

char	*my_strstr(char *str, char *to_find)
{
  int	i;

  while (*str != '\0')
    {
      i = 0;
      while (*str == to_find[i] && to_find[i] != '\0' && *str)
	{
	  i = i + 1;
	  str = str + 1;
	}
      str = str - i;
      if (to_find[i] == '\0')
	return (str);
      str = str + 1;
    }
  return (0);
}
