/*
** my_strcmp.c for DAy 06 in /home/thing-_a/rendu/Piscine-C-Jour_06/ex_05
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Mon Oct  7 18:28:07 2013 a
** Last update Thu Oct 24 14:25:07 2013 a
*/

int	my_strcmp(char *s1, char *s2)
{
  int	i;

  i = 0;
  while (s1[i] != '\0')
    {
      if (s1[i] != s2[i])
	return (s1[i] - s2[i]);
      i++;
    }
  return (s1[i] - s2[i]);
}
