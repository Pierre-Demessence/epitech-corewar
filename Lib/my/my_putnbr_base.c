/*
** my_putnbr_base.c for Day 06 in /home/thing-_a/rendu/Piscine-C-Jour_06/ex_15
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Tue Oct  8 13:37:40 2013 a
** Last update Mon Oct 14 12:13:04 2013 a
*/

#include "my.h"

int	is_neg_base(int nb)
{
  if (nb >= 0)
    nb = nb * -1;
  else
    my_putchar('-');
  return (nb);
}

int	show_nb_base(int nb, char *base, int base_len)
{
  int	i;

  i = nb / base_len;
  if (i < 0)
    show_nb_base(i, base, base_len);
  if (i != 0)
    my_putchar(base[(i % base_len) * -1]);
  return (0);
}

int	my_putnbr_base(int nbr, char *base)
{
  int	base_len;

  base_len = my_strlen(base);
  nbr = is_neg_base(nbr);
  show_nb_base(nbr, base, base_len);
  my_putchar(base[(nbr  % base_len) * -1]);
  return (0);
}
