/*
** my_strcpy.c for Day 06 in /home/thing-_a/rendu/Piscine-C-Jour_06/ex_01
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Mon Oct  7 18:04:48 2013 a
** Last update Fri Oct 11 07:22:03 2013 a
*/

char	*my_strcpy(char *dest, char *src)
{
  int	i;

  i = 0;
  while (src[i] != '\0')
    {
      dest[i] = src[i];
      i++;
    }
  dest[i] = '\0';
  return (dest);
}
