/*
** my_strncmp.c for Day 06 in /home/thing-_a/rendu/Piscine-C-Jour_06/ex_06
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Mon Oct  7 18:34:26 2013 a
** Last update Mon Oct 14 15:54:19 2013 a
*/

int	my_strncmp(char *s1, char *s2, int n)
{
  int	i;

  i = 0;
  while (i < n && s1[i])
    {
      if (s1[i] != s2[i])
	return (s1[i] - s2[i]);
      i++;
    }
  if (i == n)
    i--;
  return (s1[i] - s2[i]);
}
