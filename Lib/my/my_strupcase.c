/*
** my_strupcase.c for Day 06 in /home/thing-_a/rendu/Piscine-C-Jour_06/ex_07
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Mon Oct  7 19:58:05 2013 a
** Last update Fri Oct 11 07:24:20 2013 a
*/

char	*my_strupcase(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      if (str[i] >= 'a' && str[i] <= 'z')
	str[i] = str[i] - ('a' - 'A');
      i++;
    }
  return (str);
}
