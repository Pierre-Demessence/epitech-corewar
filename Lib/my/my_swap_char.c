/*
** my_swap_char.c for Lib in /home/thing-_a/rendu/Piscine-C-Jour_11/ex_03
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Mon Oct 21 10:48:51 2013 a
** Last update Mon Oct 21 12:11:46 2013 a
*/

int	my_swap_char(char **s1, char **s2)
{
  char	*tmp;

  tmp = *s1;
  *s1 = *s2;
  *s2 = tmp;
  return (0);
}
