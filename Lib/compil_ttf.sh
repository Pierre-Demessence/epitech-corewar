#!/bin/sh
## compil_ttf.sh for Corewar in /home/demess_p/Projects/C/Corewar/Lib
## 
## Made by demess_p
## Login   <demess_p@epitech.net>
## 
## Started on  Sun Dec 15 13:30:15 2013 demess_p
## Last update Sun Dec 15 14:49:42 2013 demess_p
##

tar -xf SDL_ttf-2.0.11.tar.gz
cd SDL_ttf-2.0.11
chmod +x configure
./configure
make
cd .libs
cp libSDL_ttf.so ../../
cd ../../
rm -rf SDL_ttf-2.0.11/
