/*
** asm.h for corewar in /home/thing-_a/rendu/git_repo/epitech-corewar/ASM
** 
** Made by a
** Login   <thing-_a@epitech.net>
** 
** Started on  Mon Dec  2 16:02:32 2013 a
** Last update Sun Dec 15 22:24:49 2013 a
*/

#ifndef			ASM_H_
# define		ASM_H_

typedef struct		s_label
{
  char			*label;
  int			addr;
  struct s_label	*next;
}			t_label;

typedef struct		s_parsed_line
{
  char			*fct_name;
  char			**tab_args;
}			t_parsed_line;


int			my_type_arg(char *arg, int n_arg);
int			my_check_if_is_index(char *fct_name, int num_arg);
int			my_check_args(t_parsed_line *parsed_line, int *prog_size,
				      int fd, t_label *label_list);
int			my_make_instruction_line(char *line, int fd,
						 int *prog_size,
						 t_label *label_list);
int			my_get_size_arg(char *arg, char *fct_name, int num_arg);

/*
** label_list_func.c
*/
int			my_open_src(char *name);
int			my_open_dest(char *name);
int			find_label_addr(t_label *label_list, char *label);
t_label			*get_label_list(int fd_src);
int			my_arg(t_parsed_line *parsed_line,
			       t_label *label_list, int tab_int[3]);
/*
** line.c
*/
int			my_get_fct_num(t_parsed_line *parsed_line);
t_parsed_line		get_parsed_line(char *line, char separator);
char			my_get_encryption_bit(char **tab_args, int nb_args);

/*
** decode_func.c
*/
void			decode_codage_byte(t_parsed_line *line, char res[4]);

/*
** tools
*/
int			my_tablen(char **tab);
char			*my_total_epur_str(char *str);
void			my_write_nbr_bites(int	nb, int nb_bits, int fd);
int			check_if_is_label(char *line);

/*
** open
*/
int			my_open_src(char *name);
int			my_open_dest(char *name);

/*
** malloc
*/
void			*my_malloc(int len);

/*
** write
*/
int			my_make_line(char *line, int fd, int *prog_size,
				     t_label *label_list);
int			my_write_header(int tab_fd[2]);

/*
** header
*/
int			my_write_datas(int fd, char *line, int result_size,
				       char *result);
int			my_write_cmd(int fd, char *line, char *element);
int			my_display_datas_header(int fd);

/*
** instruction.c
*/
int			my_make_instruction_line(char *line, int fd,
						 int *prog_size,
						 t_label *label_list);
t_parsed_line		get_parsed_line(char *line, char separator);
int			my_get_fct_num(t_parsed_line *parsed_line);
char			my_get_encryption_bit(char **tab_args, int nb_args);

/*
** encode_func.c
*/
char			*get_label_from_line(char *label);
int			my_get_line_addr(char *line, char has_label);
int			my_label_encode(int pc_addr, char *label,
					t_label *label_list);

/*
** error.c
*/
int			my_error(int num_error);

#endif		/* !ASM_H_ */
